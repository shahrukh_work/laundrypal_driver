//
//  BatchJob.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 6/3/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation

class BatchJob {
    var orders = [Orders]()
    var orderType: JobType = .pickup
    var status: HomeControllerMode = .none
    
    init(orders: [Orders], orderType: JobType, status: HomeControllerMode) {
        self.orders = orders
        self.orderType = orderType
        self.status = status
    }
}

//
//"order_id": 3,
//"description": "",
//"order_type": "pickup",
//"total_price": 26,
//"total_quantity": 0,
//"pickup_address": "abc 23 c",
//"pickup_location": "123456789,123456789",
//"customer_name": "umerck tahir",
//"pickup_datetime": "2020-06-26T19:02:12.000Z"
