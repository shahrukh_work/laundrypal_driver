//
//  OrderItems.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 6/2/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation

class OrderItem {
    var name = ""
    var description = ""
    var quantity = 0
    var price = 0
    var isLocaleAdded = false
    
    init(name: String, description: String, quantity: Int, price: Int) {
        self.name = name
        self.description = description
        self.quantity = quantity
        self.price = price
    }
}
