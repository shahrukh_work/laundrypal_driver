

import Foundation
import ObjectMapper

typealias SubCategoriesListCompletionHandler = (_ data: SubCategoriesList?, _ error: Error?,_ message: Int?) -> ()


class SubCategoriesList: Mappable {
	var error = false
	var message = ""
	var data = [SubCategoryListData]()
    var errors = [String]()
    
	required init?(map: Map) {

	}

    func mapping(map: Map) {

		error <- map["error"]
		message <- map["message"]
		data <- map["data"]
		errors <- map["errors"]
	}
    
    class func getSubCategoriesList (catId: Int, completion: @escaping SubCategoriesListCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getSubCategoriesListMethod(catId: catId) { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                let json = ["data":result]
                
                if let data = Mapper<SubCategoriesList>().map(JSONObject: json) {
                    print(data)
                    completion(data,nil,status)
                    
                } else {
                    completion(nil,error,status)
                }
                
            } else {
                completion(nil,error,status)
            }
        }
    }

}
