

import Foundation
import ObjectMapper

class SubCategoryListData : Mappable {
	var id = -1
	var name = ""
	var categoryId = -1
    var isSelected = false

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		id <- map["id"]
		name <- map["name"]
		categoryId <- map["category_id"]
	}
}
