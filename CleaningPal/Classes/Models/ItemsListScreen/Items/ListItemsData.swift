
import Foundation
import ObjectMapper

class ListItemsData : Mappable {
	var id = -1
	var itemName = ""
	var subCategoryName = ""
	var image = ""
    var itemWashingMethodString = ""
    var itemWashingMethodArray = [WashingMethods]()
    var selectedServiceIndex = 0

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		id <- map["id"]
		itemName <- map["item_name"]
		subCategoryName <- map["subcategory_name"]
		itemWashingMethodString <- map["item_washing_methods"]
		image <- map["image"]
        postMap()
	}
    
    func postMap () {
        
        if let data = Mapper<WashingMethods>().mapArray(JSONString: itemWashingMethodString) {
            itemWashingMethodArray = data
        }
    }
}
