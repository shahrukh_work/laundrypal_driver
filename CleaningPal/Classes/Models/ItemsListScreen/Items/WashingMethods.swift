//
//  WashingMethods.swift
//  CleaningPal-Driver
//
//  Created by Mac on 10/07/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

class WashingMethods: Mappable {
    var washingMethodId = ""
    var washingMethodName = ""
    var price = -1
    var qauntity = 1 //non-mapping
    var isSelected = false //non-mapping
    var subCategoryName = "" //non-mapping
    var itemId = -1 //non-mapping
    var isLocaleAdded = false

    required init?(map: Map) {

    }

    func mapping(map: Map) {
        washingMethodId <- map["washing_method_id"]
        washingMethodName <- map["washing_method_name"]
        price <- map["price"]
    }
}
