

import Foundation
import ObjectMapper

class CategoriesList : Mappable {
	var error = false
	var message = ""
    var data = Mapper<CategoriesListData>().map(JSON: [:])!
	var errors = [String]()

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		error <- map["error"]
		message <- map["message"]
		data <- map["data"]
		errors <- map["errors"]
	}

}
