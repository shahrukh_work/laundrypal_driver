

import Foundation
import ObjectMapper

typealias CategoriesListCompletionHandler = (_ data: CategoriesListData?, _ error: Error?,_ message: Int?) -> ()

class CategoriesListData : Mappable {
	var categories = [Categories]()

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		categories <- map["categories"]
	}

    class func getCategoriesList (completion: @escaping CategoriesListCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getCategoriesListMethod { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = Mapper<CategoriesListData>().map(JSON: result as! [String : Any]) {
                    completion(data,nil,status)
                    
                } else {
                    completion(nil,error,status)
                }
                
            } else {
                completion(nil,error,status)
            }
        }
    }
}
