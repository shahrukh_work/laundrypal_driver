

import Foundation
import ObjectMapper

class Categories : Mappable {
	var id = -1
	var name = ""
    var image = ""
    var isSelected = false

	required init?(map: Map) {

	}

    func mapping(map: Map) {
		id <- map["id"]
		name <- map["name"]
        image <- map["image"]
	}

}
