

import Foundation
import ObjectMapper

typealias PendingOrderCompletionHandler = (_ data: PendingData?, _ error: Error?,_ message: Int?) -> ()


class PendingOrder : Mappable {
	var error : Bool = false
	var message : String = ""
    var data = Mapper<PendingData>().map(JSON: [:])!
	var errors : [String] = []

	required init?(map: Map) {}

    func mapping(map: Map) {
		error <- map["error"]
		message <- map["message"]
		data <- map["data"]
		errors <- map["errors"]
	}
    
    class func loadPendingOrders (driverId: Int, showLoading: Bool=true, completion: @escaping PendingOrderCompletionHandler) {
        if showLoading {
            Utility.showLoading()
        }
        
        APIClient.shared.pendingOrderMethod(id: driverId) { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = Mapper<PendingData>().map(JSON: result as! [String : Any]) {
                    completion(data,nil,status)
                    
                } else {
                    completion(nil,error,status)
                }
                
            } else {
                completion(nil,error,status)
            }
        }
    }
}
