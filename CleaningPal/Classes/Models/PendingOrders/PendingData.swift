

import Foundation
import ObjectMapper

class PendingData : Mappable {
	var orders = [Orders]()

	required init?(map: Map) {}

    func mapping(map: Map) {
		orders <- map["batch"]
	}
}


