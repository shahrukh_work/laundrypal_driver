//
//  ConfrimOrder.swift
//  CleaningPal-Driver
//
//  Created by Mac on 10/07/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias ConfirmOrderCompletionHandler = (_ data: String?, _ error: Error?,_ message: Int?) -> ()

class ConfirmOrder: Mappable {
    var error : Bool = false
    var message : String = ""
    var data = ""
    var errors : [String] = []
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error <- map["error"]
        message <- map["message"]
        data <- map["data"]
        errors <- map["errors"]
    }
    
    class func confirmOrder (request: [String:Any], completion: @escaping ConfirmOrderCompletionHandler) {
        
        Utility.showLoading()
        APIClient.shared.addEasyOrderMethod(easyOrderRequest: request) { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                completion("Order Comfirmed",nil,status)
                    
            } else {
                completion(nil,error,status)
            }
        }
    }
}
