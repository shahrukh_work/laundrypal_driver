

import Foundation
import ObjectMapper


class ChatHistory : Mappable {
	var driverImage = ""
	var adminImage = ""
	var driverId = -1
	var adminId = -1
	var message = ""
	var createdAt = ""
	var sendBy = -1

	required init?(map: Map) {	}

    func mapping(map: Map) {

		driverImage <- map["driver_image"]
		adminImage <- map["admin_image"]
		driverId <- map["driver_id"]
		adminId <- map["admin_id"]
		message <- map["message"]
		createdAt <- map["created_at"]
		sendBy <- map["send_by"]
	}

}
