

import Foundation
import ObjectMapper

typealias ChatHistoryCompletionHandler = (_ data: ChatData?, _ error: Error?,_ message: Int?) -> ()


class ChatData : Mappable {
	var chatHistory = [ChatHistory]()

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		chatHistory <- map["chat_history"]
	}

    
    class func getChatHistory (completion: @escaping ChatHistoryCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getChatHistoryMethod { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = Mapper<ChatData>().map(JSON: result as! [String : Any]) {
                    completion(data,nil,status)
                    return
                }
                completion(nil,error,status)
                
            } else {
                completion(nil,error,status)
            }
        }
    }
}
