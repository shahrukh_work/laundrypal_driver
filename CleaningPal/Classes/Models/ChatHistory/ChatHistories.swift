

import Foundation
import ObjectMapper

class ChatHistories : Mappable {
	var error : Bool = false
	var message : String = ""
    var data = Mapper<ChatData>().map(JSON: [:])!
	var errors = [String]()

	required init?(map: Map) {	}

    func mapping(map: Map) {

		error <- map["error"]
		message <- map["message"]
		data <- map["data"]
		errors <- map["errors"]
	}

}
