//
//  StartOrder.swift
//  CleaningPal-Driver
//
//  Created by Mac on 10/07/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias StartOrderCompletionHandler = (_ data: String?, _ error: Error?,_ message: Int?) -> ()

class StartOrder : Mappable {
    var error : Bool = false
    var message : String = ""
    var data = ""
    var errors : [String] = []
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error <- map["error"]
        message <- map["message"]
        data <- map["data"]
        errors <- map["errors"]
    }
    
    class func setStartOrder (orderId: Int, completion: @escaping StartOrderCompletionHandler) {
        
       // Utility.showLoading()
        APIClient.shared.setStartOrderMethod(orderId: orderId) { (result, error, status) in
      //      Utility.hideLoading()
            
            if error == nil {
               // completion(result as! String,nil,status)
                
            } else {
                completion(nil,error,status)
            }
        }
    }
    
    class func setOrderPickedUp (orderId: Int, completion: @escaping StartOrderCompletionHandler) {
        
        Utility.showLoading()
        APIClient.shared.setOrderPickedUpMethod(orderId: orderId) { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                completion(result as! String,nil,status)
                
            } else {
                completion(nil,error,status)
            }
        }
    }
    
    class func setOrderDelivered (orderId: Int, collectedAmount: Double, status: Int, completion: @escaping StartOrderCompletionHandler) {
        
        Utility.showLoading()
        APIClient.shared.setOrderDeliveredMethod(orderId: orderId, collectedAmount: collectedAmount) { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                completion(result as! String,nil,status)
                
            } else {
                completion(nil,error,status)
            }
        }
    }
}
