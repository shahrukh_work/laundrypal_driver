//
//  Notification.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/27/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation

class Notification {
    var orderID = ""
    var orderStatus = ""
    var address = ""
    var time = ""
    var isNewNotification = false
    
    init(orderID: String, orderStatus: String, address: String, time: String, isNewNotification: Bool = false) {
        self.orderID = orderID
        self.orderStatus = orderStatus
        self.address = address
        self.time = time
        self.isNewNotification = isNewNotification
    }
}
