

import Foundation
import ObjectMapper

class Orders: Mappable {
	var id = -1
    var status = -1
	var type = ""
	var price = -1
	var count = -1
	var orderDetailString = ""
    var orderDetailArray = [OrdersInfo]()

	required init?(map: Map) {}

    func mapping(map: Map) {
        id                  <- map["id"]
		status              <- map["status"]
		type                <- map["type"]
		price               <- map["price"]
		count               <- map["count"]
		orderDetailString   <- map["order_details"]
        postMap()
    }
    
    func postMap () {
        
        if let data = Mapper<OrdersInfo>().mapArray(JSONString: orderDetailString.trimmingCharacters(in: .whitespacesAndNewlines)) {
            orderDetailArray = data
        }
    }
}
