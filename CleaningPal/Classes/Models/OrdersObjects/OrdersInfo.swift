//
//  OrdersInfo.swift
//  CleaningPal-Driver
//
//  Created by Mac on 08/07/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

class OrdersInfo: Mappable {
    
    var id = -1
    var pickupDate = ""
    var availableStartTime = ""
    var availableEndTime = ""
    var status = -1
    var location = ""
    var position = -1
    var address = ""
    var customerName = ""
    var description = ""
    var customerId = -1

    required init?(map: Map) {}

    func mapping(map: Map) {
        id                       <- map["id"]
        pickupDate              <- map["pickup_date"]
        availableStartTime      <- map["available_start_time"]
        availableEndTime        <- map["available_end_time"]
        status                   <- map["status"]
        description             <- map["description"]
        location                <- map["pickup_location"]
        position                <- map["position"]
        address                  <- map["pickup_address"]
        customerId              <- map["customer_id"]
        customerName            <- map["customer_name"]
    }
}
