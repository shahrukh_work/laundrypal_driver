//
//  CurrentData.swift
//  CleaningPal-Driver
//
//  Created by Mac on 06/07/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

class CurrentData : Mappable {
    var orders = [Orders]()

    required init?(map: Map) {}

    func mapping(map: Map) {
        orders <- map["current_orders"]
    }
}


