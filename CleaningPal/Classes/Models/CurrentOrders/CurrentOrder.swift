//
//  CurrentOrder.swift
//  CleaningPal-Driver
//
//  Created by Mac on 06/07/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias CurrentOrderCompletionHandler = (_ data: CurrentData?, _ error: Error?,_ message: Int?) -> ()

class CurrentOrder : Mappable {
    var error : Bool = false
    var message : String = ""
    var data = Mapper<CurrentData>().map(JSON: [:])!
    var errors : [String] = []

    required init?(map: Map) {}

    func mapping(map: Map) {
        error <- map["error"]
        message <- map["message"]
        data <- map["data"]
        errors <- map["errors"]
    }
    
    class func loadCurrentOrders (driverId: Int, showLoading: Bool=true, completion: @escaping CurrentOrderCompletionHandler) {
        
      //  Utility.showLoading()
        
        APIClient.shared.currentOrderMethod(id: driverId) { (result, error, status) in
         //   Utility.hideLoading()
            if error == nil {
                
                if let data = Mapper<CurrentData>().map(JSON: result as! [String : Any]) {
                    completion(data,nil,status)
                } else {
                    completion(nil,error,status)
                }
                
            } else {
                completion(nil,error,status)
            }
        }
    }
}
