//
//  CancelJob.swift
//  CleaningPal-Driver
//
//  Created by Mac on 10/07/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias CancelOrderCompletionHandler = (_ data: String?, _ error: Error?,_ message: Int?) -> ()

class CancelOrder : Mappable {
    var error : Bool = false
    var message : String = ""
    var data = ""
    var errors : [String] = []
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error <- map["error"]
        message <- map["message"]
        data <- map["data"]
        errors <- map["errors"]
    }
    
    class func setCancelOrder (orderId: Int, driver: Int, cancelNote: String, completion: @escaping CancelOrderCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.setCancelOrderMethod(orderId: orderId, driverId: driver, cancelNote: cancelNote) { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                completion(result as! String, nil, status)
                
            } else {
                completion(nil,error,status)
            }
        }
    }
}
