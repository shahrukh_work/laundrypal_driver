//
//  ChatTimingData.swift
//  CleaningPal-Driver
//
//  Created by Mac on 03/08/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias ChatTimingDataCompletionHandler = (_ data: ChatTimingData?, _ error: Error?,_ message: Int?) -> ()

class ChatTimingData : Mappable {
    var status = false
    var message = ""

    required init?(map: Map) {    }

    func mapping(map: Map) {

        status <- map["status"]
        message <- map["message"]
    }

    class func getChatTiming (completion: @escaping ChatTimingDataCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getChatTiming { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = Mapper<ChatTimingData>().map(JSON: result as! [String : Any]) {
                    completion(data,nil,status)
                    return
                }
                completion(nil,error,status)
                
            } else {
                completion(nil,error,status)
            }
        }
    }
}
