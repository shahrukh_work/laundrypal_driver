//
//  ChatTiming.swift
//  CleaningPal-Driver
//
//  Created by Mac on 03/08/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

class ChatTiming : Mappable {
    var error : Bool?
    var message : String?
    var data = Mapper<ChatTimingData>().map(JSON: [:])!
    var errors : [String]?

    required init?(map: Map) {

    }

    func mapping(map: Map) {

        error <- map["error"]
        message <- map["message"]
        data <- map["data"]
        errors <- map["errors"]
    }

}
