//
//  NotificationsData.swift
//  CleaningPal-Driver
//
//  Created by Mac on 06/07/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias SetNotificationsCompletionHandler = (_ data: String?, _ error: Error?,_ message: Int?) -> ()

class NotificationData: Mappable {
    var error = false
    var message = ""
    var data = ""
    var errors : [String] = []

    required init?(map: Map) {

    }

    func mapping(map: Map) {

        error <- map["error"]
        message <- map["message"]
        data <- map["data"]
        errors <- map["errors"]
    }

    
    class func setNotifications (driverId: Int, status: Bool, completion: @escaping SetNotificationsCompletionHandler) {
           
           Utility.showLoading()
           APIClient.shared.setNotificationsMethod(id: driverId, status: status) { (result, error, status) in
               Utility.hideLoading()
               
               if error == nil {
                    completion(result as? String,nil,status)
                   
               } else {
                   completion(nil,error,status)
               }
           }
       }
}
