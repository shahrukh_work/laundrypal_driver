

import Foundation
import ObjectMapper
import SocketIO
import GooglePlaces

typealias LoginCompletions = (_ data: UserData?, _ error: Error?) -> ()

class UserData : Mappable {
    
    static var shared = Mapper<UserData>().map(JSON: [:])!
    var user : User = Mapper<User>().map(JSON: [:])!
    var totalPriceForEasyOrder = 0.0
    var token : String = ""
    var status : String = ""
    var totalItemsSelected = [OrderItems]()
    var manager: SocketManager?
    var socket: SocketIOClient?
    let locationManager = CLLocationManager()
    
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        user <- map["user"]
        token <- map["token"]
        status <- map["status"]
    }
    
    class func loginAuthentication (email: String, password: String, completion: @escaping LoginCompletions) {
        
        Utility.showLoading()
        APIClient.shared.loginMethod(email: email, password: password) { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let auth = Mapper<UserData>().map(JSONObject: result) {
                    
                    if auth.status == "ACTIVE" {
                        DataManager.shared.setAuthentication(auth: auth.toJSONString()!)
                        DataManager.shared.setUser(user: auth.toJSONString()!)
                    }
                    UserData.shared = auth
                    completion(auth, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}
