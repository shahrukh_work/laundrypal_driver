

import Foundation
import ObjectMapper

class User : Mappable {
	var id : Int = -1
	var firstName : String = ""
	var lastName : String = ""
	var email : String = ""
	var phone : String = ""
	var image : String = ""
	var address : String = ""
	var city : String = ""
	var province : String = ""
	var postalCode : Int = -1
	var vehicleModel : Int = -1
	var vehicleType : String = ""
	var vehicleColor : String = ""
	var vehicleLicenseNumber : String = ""
	var drivingLicense : String = ""
	var insuranceCoverage : String = ""
	var status : Int = -1
	var socketStatus : Int = -1
	var createdBy : Int = -1
	var createdAt : String = ""
	var updatedAt : String = ""
	var notifications : Int = -1

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		id <- map["id"]
		firstName <- map["first_name"]
		lastName <- map["last_name"]
		email <- map["email"]
		phone <- map["phone"]
		image <- map["image"]
		address <- map["address"]
		city <- map["city"]
		province <- map["province"]
		postalCode <- map["postal_code"]
		vehicleModel <- map["vehicle_model"]
		vehicleType <- map["vehicle_type"]
		vehicleColor <- map["vehicle_color"]
		vehicleLicenseNumber <- map["vehicle_license_number"]
		drivingLicense <- map["driving_license"]
		insuranceCoverage <- map["insurance_coverage"]
		status <- map["status"]
		socketStatus <- map["socket_status"]
		createdBy <- map["created_by"]
		createdAt <- map["created_at"]
		updatedAt <- map["updated_at"]
		notifications <- map["notifications"]
	}

}
