

import Foundation
import ObjectMapper

class NotificationLists : Mappable {
	var error : Bool?
	var message : String?
	var data : Data?
	var errors : [String]?

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		error <- map["error"]
		message <- map["message"]
		data <- map["data"]
		errors <- map["errors"]
	}

}
