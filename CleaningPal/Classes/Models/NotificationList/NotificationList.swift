

import Foundation
import ObjectMapper

class NotificationList : Mappable {
	var id = -1
	var batchId = -1
    var type = ""
    var startLocation = ""
    var endLocation = ""
	var assignedStatus = -1
	var status = -1
	var title = ""
	var message = ""
	var driverId = -1
	var createdAt = ""
	var updatedAt = ""
    var isNewNotification = false

	required init?(map: Map) {

	}

    func mapping (map: Map) {

		id                    <- map["id"]
        startLocation        <- map["start_location"]
        endLocation          <- map["end_location"]
        type                 <- map["type"]
		batchId              <- map["batch_id"]
		assignedStatus      <- map["assigned_status"]
		status               <- map["status"]
		title                <- map["title"]
		message              <- map["message"]
		driverId             <- map["driver_id"]
		createdAt            <- map["created_at"]
		updatedAt            <- map["updated_at"]
	}

}
