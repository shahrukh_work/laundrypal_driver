

import Foundation
import ObjectMapper

typealias NotificationListCompletionHandler = (_ data: NotificationListData?, _ error: Error?,_ message: Int?) -> ()


class NotificationListData : Mappable {
	var notificationList = [NotificationList]()

	required init?(map: Map) {}

    func mapping(map: Map) {
		notificationList <- map["notification_list"]
	}

class func notificationList (limit: Int = 10, completion: @escaping NotificationListCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getNotificationListMethod(limit: limit) { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = Mapper<NotificationListData>().map(JSON: result as! [String : Any]) {
                    completion(data,nil,status)
                    return
                }
                completion(nil,error,status)
                
            } else {
                completion(nil,error,status)
            }
        }
    }
}
