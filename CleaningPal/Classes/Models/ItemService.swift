//
//  ItemService.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 6/2/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation

class ItemService {
    var name = ""
    var price = ""
    var isSelected = false
    
    init(name: String, price: String) {
        self.name = name
        self.price = price
    }
}
