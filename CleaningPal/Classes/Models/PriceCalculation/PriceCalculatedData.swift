

import Foundation
import ObjectMapper

class PriceCalculatedData : Mappable {
	var items = [PricedItems]()
    var totalPrice = 0.0
	var totalCount = "0"
	var promo = ""
	var gst = ""
	var discountedPrice = 0.0
	var gstRate = -1

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		items <- map["items"]
		totalPrice <- map["total_price"]
		totalCount <- map["total_count"]
		promo <- map["promo"]
		gst <- map["gst"]
		discountedPrice <- map["discounted_price"]
		gstRate <- map["gst_rate"]
	}

    
}
