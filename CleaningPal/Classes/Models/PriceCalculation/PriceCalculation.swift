

import Foundation
import ObjectMapper

typealias PriceCalculationCompletionHandler = (_ data: PriceCalculatedData?, _ error: Error?,_ message: Int?) -> ()

class PriceCalculation : Mappable {
	var error : Bool?
	var message : String?
    var data = Mapper<PriceCalculatedData>().map(JSON: [:])!
	var errors : [String]?

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		error <- map["error"]
		message <- map["message"]
		data <- map["data"]
		errors <- map["errors"]
	}
    
    class func getCalculatedPrice (request: [String:Any], completion: @escaping PriceCalculationCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.calcualtePriceMethod(request: request) { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = Mapper<PriceCalculatedData>().map(JSON: result as! [String : Any]) {
                    completion(data,nil,status)
                    return
                }
                completion(nil,error,status)
                
            } else {
                completion(nil,error,status)
            }
        }
    }

}
