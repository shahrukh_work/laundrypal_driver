
import Foundation
import ObjectMapper

class PricedItems : Mappable {
	var itemId = -1
	var price = -1
	var washingMethod = ""
	var quantity = -1

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		itemId <- map["item_id"]
		price <- map["price"]
		washingMethod <- map["washing_method"]
		quantity <- map["quantity"]
	}

}
