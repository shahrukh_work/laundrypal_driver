//
//  CompletedOrder.swift
//  CleaningPal-Driver
//
//  Created by Mac on 06/07/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias CompletedOrderCompletionHandler = (_ data: CompletedData?, _ error: Error?,_ message: Int?) -> ()

class CompletedOrder : Mappable {
    var error : Bool = false
    var message : String = ""
    var data = Mapper<CompletedData>().map(JSON: [:])!
    var errors : [String] = []

    required init?(map: Map) {}

    func mapping(map: Map) {
        error <- map["error"]
        message <- map["message"]
        data <- map["data"]
        errors <- map["errors"]
    }
    
    class func loadCompletedOrders (driverId: Int, showLoading: Bool=true, completion: @escaping CompletedOrderCompletionHandler) {
       
        if showLoading {
            Utility.showLoading()
        }
        
        APIClient.shared.completedOrderMethod(id: driverId) { (result, error, status) in
            
            Utility.hideLoading()
            if error == nil {
                
                if let data = Mapper<CompletedData>().map(JSON: result as! [String : Any]) {
                    completion(data,nil,status)
                } else {
                    completion(nil,error,status)
                }
                
            } else {
                completion(nil,error,status)
            }
        }
    }
}
