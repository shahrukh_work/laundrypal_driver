//
//  Arrived.swift
//  CleaningPal-Driver
//
//  Created by Mac on 08/07/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias DriverArrivedCompletionHandler = (_ data: String?, _ error: Error?,_ message: Int?) -> ()

class DriverArrivedData: Mappable {
    var error = false
    var message = ""
    var data = ""
    var errors : [String] = []

    required init?(map: Map) {

    }

    func mapping(map: Map) {

        error <- map["error"]
        message <- map["message"]
        data <- map["data"]
        errors <- map["errors"]
    }

    
    class func setDriverArrived (orderId: Int, customerId: Int, completion: @escaping DriverArrivedCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.driverArrivedMethod(orderId: orderId, customerId: customerId) { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                completion(result as! String, nil,status)
                
            } else {
                completion(nil,error,status)
            }
        }
    }
}
