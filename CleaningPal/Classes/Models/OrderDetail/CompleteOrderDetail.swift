

import Foundation
import ObjectMapper

typealias OrderDetailCompletionHandler = (_ data: OrderDetailData?, _ error: Error?,_ message: Int?) -> ()

class CompleteOrderDetail : Mappable {
	var error = false
	var message = ""
    var data = Mapper<OrderDetailData>().map(JSON: [:])!
	var errors = [String]()

	required init?(map: Map) {}

    func mapping(map: Map) {
		error           <- map["error"]
		message         <- map["message"]
		data            <- map["data"]
		errors          <- map["errors"]
	}
    
    class func loadOrderDetail (orderId: Int, completion: @escaping OrderDetailCompletionHandler) {
        
     //   Utility.showLoading()
        APIClient.shared.orderDetailMethod(id: orderId) { (result, error, status) in
          //  Utility.hideLoading()
            
            if error == nil {
                
                if let data = Mapper<OrderDetailData>().map(JSON: result as! [String : Any]) {
                    completion(data,nil,status)
                } else {
                    completion(nil,error,status)
                }
                
            } else {
                completion(nil,error,status)
            }
        }
    }
}
