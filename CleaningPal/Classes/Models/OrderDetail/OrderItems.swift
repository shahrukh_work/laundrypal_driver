

import Foundation
import ObjectMapper

class OrderItems : Mappable {
	var name = ""
	var washingMethod = ""
	var price = -1
	var quantity = -1
	var subcategoryName = ""
    var itemId = -1 // non-mapping
    var isLocaleAdded = false

	required init?(map: Map) {

	}

    func mapping(map: Map) {
        itemId              <- map["id"]
		name                <- map["name"]
		washingMethod       <- map["washing_method"]
		price               <- map["price"]
		quantity            <- map["quantity"]
		subcategoryName     <- map["subcategory_name"]
	}

}
