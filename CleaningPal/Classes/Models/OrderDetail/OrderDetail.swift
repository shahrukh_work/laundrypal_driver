

import Foundation
import ObjectMapper

class OrderDetail : Mappable {
	var description = ""
	var orderId = -1
	var status = -1
	var orderType = ""
    var discountedPrice = -1.0
	var pickupDate = ""
	var pickupAddress = ""
    var pickupLocation = ""
    var totalPrice = -1.0
	var totalQuantity = -1
    var customerId = -1
	var customerName = ""
	var phone = ""
	var email = ""
	var driverName = ""
	var vehicleType = ""
	var vehicleColor = ""
    var orderCategory = ""
    var gst = -1
    var paymentStatus = false
    var amountPaid = 0.0
    var starchDescription = "" //non-mapping
    var isStarch = false //non-mapping

	required init?(map: Map) {

	}

     func mapping(map: Map) {
        amountPaid              <- map["amount_paid"]
		description             <- map["description"]
		orderId                 <- map["order_id"]
		status                  <- map["status"]
		orderType               <- map["order_type"]
		discountedPrice         <- map["discounted_price"]
		pickupDate              <- map["pickup_date"]
		pickupAddress           <- map["pickup_address"]
        pickupLocation          <- map["pickup_location"]
		totalPrice              <- map["total_price"]
		totalQuantity           <- map["total_quantity"]
        customerId              <- map["customer_id"]
		customerName            <- map["customer_name"]
		phone                   <- map["phone"]
		email                   <- map["email"]
		driverName              <- map["driver_name"]
		vehicleType             <- map["vehicle_type"]
		vehicleColor            <- map["vehicle_color"]
        orderCategory           <- map["order_category"]
        gst                     <- map["gst"]
        paymentStatus            <- map["payment_status"]
	}

}
