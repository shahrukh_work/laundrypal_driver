

import Foundation
import ObjectMapper

class OrderDetailData : Mappable {
    
    var orderDetail = Mapper<OrderDetail>().map(JSON: [:])!
	var orderItems = [OrderItems]()
    var isArrived = false //Non-Mapping

	required init?(map: Map) {}

    func mapping(map: Map) {

		orderDetail <- map["order_detail"]
		orderItems <- map["order_items"]
	}

}
