//
//  ItemsRequest.swift
//  CleaningPal-Driver
//
//  Created by Mac on 30/07/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation

class RequestItems {
    var itemId = -1
    var price = 0
    var washingMethod = ""
    var quantity = -1
}
