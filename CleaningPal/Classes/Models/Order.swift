//
//  Order.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 6/2/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation

class Order {
    var customerName = ""
    var orderId = 0
    var time = ""
    var description = ""
    var address = ""
    var orderType: JobType = .pickup
    var orderItems = [OrderItem]()
    
    init(customerName: String, orderId: Int, time: String, description: String, address: String, orderType: JobType, orderItems: [OrderItem]) {
        self.customerName = customerName
        self.orderId = orderId
        self.time = time
        self.description = description
        self.address = address
        self.orderType = orderType
        self.orderItems = orderItems
    }
}
