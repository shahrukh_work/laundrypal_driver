
import Foundation
import ObjectMapper

typealias AdminDataCompletionHandler = (_ data: AdminData?, _ error: Error?,_ message: Int?) -> ()

class AdminData : Mappable {
	var id = -1
	var name = ""
	var image = ""

	required init?(map: Map) {	}

    func mapping(map: Map) {

		id <- map["id"]
		name <- map["name"]
		image <- map["image"]
	}

    class func getOnlineAdmin (completion: @escaping AdminDataCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getOnlineAdminMethod { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = Mapper<AdminData>().map(JSON: (result as? [String : Any])!) {
                    completion(data,nil,status)
                    return
                }
                completion(nil,error,status)
                
            } else {
                completion(nil,error,status)
            }
        }
    }
}
