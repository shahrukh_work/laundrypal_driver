//
//  Constants.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 3/17/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

let kApplicationWindow = Utility.getAppDelegate()!.window
let kFcmToken = Utility.getAppDelegate()?.token
var showLoader = false
var showAlertDelievery = false


struct APIRoutes {
    static var imageBaseURL = "https://cleaningpal-backend.codesorbit.com/api/rider/driverImage"
    static var baseUrl = "https://cleaningpal-backend.codesorbit.com"
    static let adminBaseURL = "/api/admin/adminImages"
    static let itemsImageBaseURL = "/api/admin/itemImages"
    static let categoryImageBaseURL = "/api/admin/categoryImages/"
    static let login = "/api/authorization/auth/driverLogin"
    static let logout = "/api/authorization/auth/driverLogout"
    static let pendingOrder = "/api/rider/order/getPendingOrders"
    static let completedOrder = "/api/rider/order/getCompletedOrders"
    static let currentOrder = "/api/rider/order/getCurrentOrders"
    static let setNotifications = "/api/rider/settings/setNotification"
    static let driverArrived = "/api/rider/order/riderArrived"
    static let orderDetail = "/api/rider/order/getOrderDetails"
    static let getCategoriesList = "/api/rider/item/categoriesList"
    static let getSubCategoriesList = "/api/rider/item/subcategoryList"
    static let getItemsList = "/api/rider/item/itemListBySubcategory"
    static let startOrder = "/api/rider/order/startOrder"
    static let startJob = "/api/rider/order/startBatch"
    static let cancelJob = "/api/rider/order/cancelOrder"
    static let confirmOrder = "/api/rider/order/addEasyOrderDetails"
    static let completeJob = "/api/rider/order/completeBatch"
    static let orderDelivered = "/api/rider/order/completeOrder"
    static let orderPickedUp = "/api/rider/order/orderPickup"
    static let notificationList = "/api/rider/settings/getNotificationList"
    static let updateToken = "/api/rider/user/updateFcmToken"
    static let searchItems = "/api/rider/item/searchItems"
    static let onlineAdmin = "/api/rider/chat/getOnlineAdmin"
    static let chatTiming = "/api/rider/chat/getChatTiming"
    static let chatHistory = "/api/rider/chat/getChatHistoy"
    static let userUpdateLoc = "/api/rider/user/updateLocation"
    static let getEasyOrderDetail = "/api/rider/order/getEasyOrderDetails"
    static let gtsRate = ""
    static let updateDistance = "/api/rider/order/updateDistance"
    static let calculatePayment = "/api/rider/payment/calculatePayment"
    static let priceCalculation = "/api/rider/order/priceCalutation"
    static let clearNotification = "/api/rider/settings/clearNotification"
}

let kGoogleMapsAPI = "AIzaSyDjjNiKps3y27gM7TpL6cyxR01YBnvKcOQ"

func googleDirectionURL(origin: String, destination: String) -> String {
    return "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(kGoogleMapsAPI)"
}

enum GradientDirection {
    case leftToRight
    case rightToLeft
    case topToBottom
    case bottomToTop
}

enum HomeControllerMode {
    case none
    case complete
    case pending
    case current
}

enum JobType: String {
    case pickup = "Pick up"
    case deliver = "Deliver"
}
