//
//  CategoryCollectionViewCell.swift
//  CleaningPal
//
//  Created by Mac on 14/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import SDWebImage

class CategoryCollectionViewCell: UICollectionViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var selectionBubbleImage: UIImageView!


    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Configure
    func configure (category: Categories) {
        nameLabel.text = category.name
        
        if let url = URL(string: APIRoutes.baseUrl + APIRoutes.categoryImageBaseURL + category.image) {
            itemImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "categoryPlaceholder"))
        }
        //selectionBubbleImage.isHidden = !category.isSelected
        itemImageView.alpha = 0.6
        nameLabel.alpha = 0.6
        if category.isSelected {
            itemImageView.alpha = 1.0
            nameLabel.alpha = 1.0
        }
    }
}
