//
//  HomeViewController.swift
//  CleaningPal
//
//  Created by Mac on 14/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper

class ItemsViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var typeCollectionView: UICollectionView!
    @IBOutlet weak var showCollectionButton: UIButton!
    @IBOutlet weak var showListButton: UIButton!
    @IBOutlet weak var itemTableView: UITableView!
    @IBOutlet weak var itemCollectionView: UICollectionView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var noItemsView: UIView!
    

    //MARK: - Variable
    var catDataSource = Mapper<CategoriesListData>().map(JSON: [:])!
    var subCatDataSource = Mapper<SubCategoriesList>().map(JSON: [:])!
    var itemsDataSource = Mapper<ListItems>().map(JSON: [:])!
    var typeDataSource: [PersonType] = []
    var lastContentOffset: CGFloat = 0
    var isSearchStarted = false
    var offset = 0
    var selectedSubCatID = -1
    var searchText = ""

    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView ()
    }
    
    
    //MARK: - Setup
    func setupView () {
        let alignedFlowLayout = AlignedCollectionViewFlowLayout(horizontalAlignment: .left, verticalAlignment: .top)
        typeCollectionView.collectionViewLayout = alignedFlowLayout
        searchTextField.delegate = self
        bottomContainerView.cornerRadius = 20
        bottomContainerView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
        self.categoryCollectionView.delegate = self
        self.categoryCollectionView.dataSource = self
        self.typeCollectionView.delegate = self
        self.typeCollectionView.dataSource = self
        
        self.itemTableView.delegate = self
        self.itemTableView.dataSource = self
        self.itemCollectionView.delegate = self
        self.itemCollectionView.dataSource = self
        
        showListButton.setImage(#imageLiteral(resourceName: "list_selected"), for: .normal)
        showCollectionButton.setImage(#imageLiteral(resourceName: "collection_unselected"), for: .normal)
        Utility.setPlaceHolderTextColor(searchTextField, "Search", #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.8490475171))
        dataSource ()
    }
    
    
    //MARK: - DataSource
    func dataSource () {
        getCategories()
    }
    
    
    //MARK: - Actions
    @IBAction func listPressed(_ sender: Any) {
        showListButton.setImage(#imageLiteral(resourceName: "list_selected"), for: .normal)
        showCollectionButton.setImage(#imageLiteral(resourceName: "collection_unselected"), for: .normal)
        itemTableView.isHidden = false
        itemCollectionView.isHidden = true
    }
    
    @IBAction func collectionPressed(_ sender: Any) {
        showListButton.setImage(#imageLiteral(resourceName: "list_unselected"), for: .normal)
        showCollectionButton.setImage(#imageLiteral(resourceName: "collection_selected"), for: .normal)
        itemTableView.isHidden = true
        itemCollectionView.isHidden = false
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: - Methods
    func getCategories () {
        
        CategoriesListData.getCategoriesList {[weak self] (result, error, status) in
            
            if error == nil {
                
                if result?.categories.count ?? 0 > 0 {
                    result?.categories[0].isSelected = true
                    self?.getSubCategories (categoryId:  result?.categories[0].id ?? -1)
                    self?.catDataSource = result!
                    self?.categoryCollectionView.reloadData()
                }
                
            } else {
                Utility.showAlertController(self!, error?.localizedDescription ?? "")
            }
        }
    }
    
    func getSubCategories (categoryId: Int) {
        
        SubCategoriesList.getSubCategoriesList(catId: categoryId) {[weak self] (result, error, status) in
            
            if error == nil {
                
                if result?.data.count ?? 0 > 0 {
                    result?.data[0].isSelected = true
                    self?.selectedSubCatID = result?.data[0].id ?? -1
                    self?.getListsItems(subCat: self?.selectedSubCatID ?? -1)
                    self?.subCatDataSource = result!
                    self?.typeCollectionView.reloadData()
                }
                
            } else {
                Utility.showAlertController(self!, error?.localizedDescription ?? "")
            }
        }
    
    }
    
    func getListsItems (subCat: Int, searchText: String = "", offset: Int = 0) {
        
        ListItems.getListItems(searchText: searchText, subCatId: subCat, offset: offset) {[weak self] (result, error, status) in
            
            if searchText != "" {
                self?.itemsDataSource.data.removeAll()
            }
            self?.isSearchStarted = false
            
            if error == nil {
                
                if let data = result?.data {
                    
                    data.forEach { (item) in
                        item.itemWashingMethodArray.first?.isSelected = true
                    }
                    
                    self?.itemsDataSource.data.append(contentsOf: data)
                    self?.itemTableView.reloadData()
                    self?.itemCollectionView.reloadData()
                }
                
                
            } else {
                Utility.showAlertController(self!, error?.localizedDescription ?? "")
            }
        }
    }
}


//MARK: - UICollectionViewDelegate & UICollectionViewDataSource
extension ItemsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.categoryCollectionView {
            return self.catDataSource.categories.count
        }
        
        if collectionView == itemCollectionView {
            noItemsView.isHidden = !itemsDataSource.data.isEmpty
            return itemsDataSource.data.count
        }
        return subCatDataSource.data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.categoryCollectionView {
            let cell = collectionView.register(CategoryCollectionViewCell.self, indexPath: indexPath)
            cell.configure(category: catDataSource.categories[indexPath.row])
            return cell
            
        }
        
        if collectionView == self.itemCollectionView {
            let cell = collectionView.register(CategoryItemCollectionViewCell.self, indexPath: indexPath)
            cell.config(data: itemsDataSource.data[indexPath.row], indexPath: indexPath)
            cell.delegate =  self
            return cell
        }
        
        let cell = collectionView.register(TypeCollectionViewCell.self, indexPath: indexPath)
        cell.configure(person: self.subCatDataSource.data[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        offset = 0
        if collectionView == categoryCollectionView {
            self.itemsDataSource.data.removeAll()

            for item in catDataSource.categories {
                item.isSelected = false
            }
            self.catDataSource.categories[indexPath.row].isSelected = true
            collectionView.reloadData()
            getSubCategories(categoryId: catDataSource.categories[indexPath.row].id)
            return
        }
        
        if collectionView == typeCollectionView {
            self.itemsDataSource.data.removeAll()
            
            for item in subCatDataSource.data {
                item.isSelected = false
            }
            self.selectedSubCatID = subCatDataSource.data[indexPath.row].id
            getListsItems(subCat: self.selectedSubCatID)
            subCatDataSource.data[indexPath.row].isSelected = true
            collectionView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == categoryCollectionView {
            return CGSize(width: 80, height: 80)
        }

        if collectionView == itemCollectionView {
            let padding: CGFloat =  0
            let collectionViewSize = collectionView.frame.size.width + padding
            return CGSize (width: collectionViewSize/2, height: 206)
        }
        
        let label = UILabel(frame: CGRect.zero)
        label.text = subCatDataSource.data[indexPath.row].name
        label.sizeToFit()
        return CGSize(width: label.frame.width + 20, height: 32)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == itemCollectionView  {
            return 0
        }
        return 10
    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension ItemsViewController: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        noItemsView.isHidden = !itemsDataSource.data.isEmpty
        return itemsDataSource.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(CategoryItemTableViewCell.self, indexPath: indexPath)
        cell.config(data: itemsDataSource.data[indexPath.row], indexPath: indexPath)
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
    }
    
     func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            print(" you reached end of the table")
            
            if searchText == "" && !isSearchStarted {
                offset += 1
                isSearchStarted = true
                self.getListsItems(subCat: self.selectedSubCatID ,searchText: searchText, offset: offset)
            }
            
        }
    }
}


//MARK: - UITextFieldDelegate
extension ItemsViewController: UITextFieldDelegate {
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
        if textField.text?.count ?? 0 > 2 {
            isSearchStarted = true
        }
        
        if isSearchStarted && textField.text?.count ?? 0 != 0 {
            
            for item in subCatDataSource.data {
            
                if item.isSelected {
                    offset = 0
                    searchText = textField.text!
                    getListsItems(subCat: item.id, searchText: textField.text!, offset: offset)
                }
            }
        }
    }
}


//MARK: - ItemServiceSelectionViewControllerDelegate
extension ItemsViewController: ItemServiceSelectionViewControllerDelegate {
    
    func addPressed(data: OrderItems) {
        UserData.shared.totalItemsSelected.append(data)
        //self.dismiss(animated: true, completion: nil)
    }
}


//MARK: - CategoryDelegate
extension ItemsViewController: CategoryItemCellDelegate {
    
    func addTapped(indexPath: IndexPath) {
        let controller = ItemServiceSelectionViewController()
        controller.item = itemsDataSource.data[indexPath.row]
        controller.dataSource = itemsDataSource.data[indexPath.row].itemWashingMethodArray
        controller.subCategoryName = itemsDataSource.data[indexPath.row].subCategoryName
        controller.delegate = self
        controller.modalPresentationStyle = .overCurrentContext
        self.present(controller, animated: true, completion: nil)
    }
    
    func serviceSelected(selectedServiceType: Int, indexPath: IndexPath) {
        
        itemsDataSource.data[indexPath.row].selectedServiceIndex = selectedServiceType
        
        itemsDataSource.data[indexPath.row].itemWashingMethodArray.forEach { (method) in
            method.isSelected = false
        }
        itemsDataSource.data[indexPath.row].itemWashingMethodArray[selectedServiceType].isSelected = true
        itemTableView.reloadData()
        itemCollectionView.reloadData()
    }
}
