//
//  MapsViewController.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 4/24/20.
//  Copyright © 2020 Bilal Saeed. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import GooglePlaces
import IQKeyboardManagerSwift
import ObjectMapper

enum OrderStatus: Int {
    case pending = -1
    case cancel = 0
    case inProgress = 1
    case completed = 2
    case delete = 3
    case arrived = 4
    case readyToDeliver = 5
    case rejected = 6
    case pickup = 8
}

class MapsViewController: UIViewController {
    
    
    //MARK: - Variables
    var lat = 0.0
    var long = 0.0
    var gameTimer: Timer?
    var jobId = -1
    var marker = GMSMarker()
    var myLocation: CLLocation?
    var orders: Orders!
    var ordersStack = [OrdersInfo]()
    var sourceView: UIView?
    var orderDetail = Mapper<OrderDetailData>().map(JSON: [:])!
    var isMapSetCurrentLocation = false
    var colors: [UIColor] = []
    var isPolylineDrawn = false
    var delegate: MapsViewController?
    var collectedAmount = 0.0
    var isReadyToSendLocation = false
    var item = [String]()
    var drawPolygon = false
    var timesForDistance = 0
    var timesForPolygon = 0
    
    
    //MARK: - Outlets
    @IBOutlet weak var notificationRedView: UIView!
    @IBOutlet weak var mapsView: GMSMapView!
    @IBOutlet weak var onJobView: OnJobView!
    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var sendMessageViewBottomConstraint: NSLayoutConstraint!
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
        setupView()
        checkWhatOrderToStart ()
        locationSetup()
        Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(sendLocation), userInfo: nil, repeats: true)
        SlideMenuOptions.panFromBezel = false

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SlideMenuOptions.panFromBezel = true
    }
    
    
    //MARK: - View Setup
    func setupView() {
        
        if let styleURL = Bundle.main.url(forResource: "MapStyle", withExtension: "json") {
            
            do {
                mapsView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                
            } catch {
                print(error.localizedDescription)
            }
        } else {
            NSLog("Unable to find MapStyle.json")
        }
        
        mapsView.cornerRadius = 24
        mapsView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        onJobView.delegate = self
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeUp.direction = .up
        self.onJobView.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
        self.onJobView.addGestureRecognizer(swipeDown)
    }
    
    func locationSetup() {
        UserData.shared.locationManager.delegate = self
        mapsView.delegate = self
        
        switch(CLLocationManager.authorizationStatus()) {
            
        case .restricted, .denied:
            self.showOkAlertWithOKCompletionHandler("Application uses location data for navigations and maps. Kindly enable location services in order to use the application.") { ( _) in
                UIApplication.shared.open(NSURL(string:UIApplication.openSettingsURLString)! as URL)
            }
            
        case .authorizedAlways, .authorizedWhenInUse:
            UserData.shared.locationManager.startUpdatingLocation()
            UserData.shared.locationManager.startUpdatingHeading()
            
        case .notDetermined:
            UserData.shared.locationManager.requestWhenInUseAuthorization()
            
        default:
            break
        }
    }
    
    
    //MARK: - Selectors
    @objc func sendLocation () {
        isReadyToSendLocation = true
        isMapSetCurrentLocation = false
    }
    
    @objc func runTimedCode () {
        UserData.shared.socket?.emit("share-location", ["driver_id" : UserData.shared.user.id, "latitude" : lat, "longitude" : long])
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        
        if gesture.direction == .up {
            self.onJobView.bottomDetailsView.isHidden = false
            self.viewHeightConstraint.constant = 269
            self.onJobView.layoutIfNeeded()
            
            UIView.animate(withDuration: 0.1) {
                self.view.layoutIfNeeded()
            }
            
        } else if gesture.direction == .down {
            self.onJobView.bottomDetailsView.isHidden = true
            self.viewHeightConstraint.constant = 88.5
            self.onJobView.layoutIfNeeded()
            
            UIView.animate(withDuration: 0.1) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    
    //MARK: - Actions
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func bellButtonTapped(_ sender: Any) {
        let controller = NotifiationsViewController()
        controller.modalPresentationStyle = .overFullScreen
        self.present(controller, animated: true, completion: nil)
    }
    
    
    //MARK: - Methods
    func drawPathAndCalculateDistance () {
        
        if item.first != "" {
            drawPolygon = true
            self.calculateTimeDistance(src: CLLocationCoordinate2D(latitude: self.myLocation?.coordinate.latitude ?? 0.0, longitude: self.myLocation?.coordinate.longitude ?? 0.0), dst: CLLocationCoordinate2D(latitude: Double(self.item[0])!, longitude: Double(self.item[1])!))
            self.createPath(location: myLocation!)
        }
    }
    
    func checkWhatOrderToStart () {
        drawPolygon = false
        Utility.showLoading()
        self.ordersStack.removeAll()
        
        if orders != nil {
            
            for order in orders.orderDetailArray {
                
                if order.status == OrderStatus.arrived.rawValue || order.status == OrderStatus.inProgress.rawValue || order.status == OrderStatus.pending.rawValue || order.status == OrderStatus.readyToDeliver.rawValue {
                    ordersStack.append(order)
                }
            }
            
            if ordersStack.count != 0 {
                let arrivedOrder = ordersStack.filter({$0.status == OrderStatus.arrived.rawValue})
                
                if arrivedOrder.count == 0 {
                    
                    let inProgressOrder = ordersStack.filter({$0.status == OrderStatus.inProgress.rawValue})
                    
                    if inProgressOrder.count > 0 {
                        getOrderDetail(orderId: inProgressOrder.first?.id ?? -1)
                        
                    } else {
                        
                        let orderSorted = ordersStack.sorted { (position1, position2) -> Bool in
                            
                            if position1.position < position2.position {
                                return true
                            }
                            return false
                        }
                        
                        startOrder(orderId: orderSorted.first?.id ?? -1 )
                    }
                    
                } else {
                    getOrderDetail(orderId: arrivedOrder.first?.id ?? -1)
                }
                
            } else {
                completeJob ()
            }
            
        } else {
             cancelCompleteJob()
        }
    }
    
    func startOrder (orderId: Int) {
        getCurrentList(driverId: UserData.shared.user.id)
        
        StartOrder.setStartOrder(orderId: orderId) { (result, error, status) in
            
            if error == nil {
                print("Order completed!")
                self.getOrderDetail(orderId: orderId)
                
            } else {
                Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
    
    func completeJob () {
        
        CompleteJob.completeJob(jobId: orders.id) { (result, error, status) in
            
            if error == nil {
                print("Job completed!")
                self.navigationController?.popViewController(animated: true)
            } else {
                Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
    
    func cancelCompleteJob () {
        
        CompleteJob.completeJob(jobId: self.jobId) { (result, error, status) in
            
            if error == nil {
                print("Job completed!")
                self.navigationController?.popViewController(animated: true)
            } else {
                Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
    
    func getOrderDetail (orderId: Int) {
        
        CompleteOrderDetail.loadOrderDetail(orderId: orderId) {[weak self] (result, error, status) in
            
            if error == nil {
                self?.orderDetail = result!
                self?.onJobView.configure(order: (self?.orderDetail.orderDetail)!)
                self?.isPolylineDrawn = false
                self?.checkDriverHasArrived()
                
            } else {
                Utility.showAlertController(self!, error?.localizedDescription ?? "")
            }
        }
    }
    
    func setArrived () {
        
        UIView.animate(withDuration: 1.0) {
            self.onJobView.arriveButton.alpha = 1.0
            self.onJobView.cancelButton.setTitle("Not Appeared", for: .normal)
            self.onJobView.arriveButton.isUserInteractionEnabled = true
        }
    }
    
    func setNotArrived () {
        
        UIView.animate(withDuration: 1.0) {
            self.onJobView.arriveButton.alpha = 0.5
            self.onJobView.cancelButton.setTitle("Cancel Order", for: .normal)
            self.onJobView.arriveButton.isUserInteractionEnabled = false
        }
    }
    
    func getCurrentList (driverId: Int) {
        
        CurrentOrder.loadCurrentOrders(driverId: driverId) {[weak self] (data, error, message) in
            
            if error == nil {
                self?.orders.orderDetailArray.removeAll()
                self?.orders =  data?.orders.first
                self?.checkWhatOrderToStart ()

            } else {
                Utility.showAlertController(self!, error?.localizedDescription ?? "")
            }
        }
    }
    
    private func calculateTimeDistance(src: CLLocationCoordinate2D, dst: CLLocationCoordinate2D){
        let origin = "\(src.latitude),\(src.longitude)"
        let destination = "\(dst.latitude),\(dst.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(kGoogleMapsAPI)"
        
        Alamofire.request(url).responseJSON { [weak self] response in
            Utility.hideLoading()
            do {
                let json = try JSON(data: response.data!)
             //   print(json)
                let route = json["routes"].arrayValue.first
                
                if let route = route {
                    let leg = route["legs"].arrayValue.first
                    
                    if let leg = leg {
                        let distance = leg["distance"].dictionaryObject?["text"]
                        
                        if let distance = distance as? String {
                           
                            self?.onJobView.distanceLeftLabel.text = "\(distance) away"
                            let distanceNew = distance.components(separatedBy: " ")
                            
                            if self?.orderDetail.orderDetail.status == OrderStatus.inProgress.rawValue {
                                
                                if distanceNew[1] == "m" {
                                    self?.updateDistance(distance: "\(Double(distanceNew[0])!)")
                                    
                                    if Double(distanceNew[0])! < 200 {
                                        self?.setArrived ()
                                        
                                    } else {
                                        self?.setNotArrived ()
                                    }
                                    
                                } else {
                                    self?.updateDistance(distance: "\(Double(distanceNew[0])! * 1000)")
                                    
                                    if Double(distanceNew[0])! * 1000 < 200 {
                                        self?.setArrived ()
                                        
                                    } else {
                                        self?.setNotArrived ()
                                    }
                                }
                                
                            } else if self?.orderDetail.orderDetail.status == OrderStatus.arrived.rawValue{
                                self?.setArrived ()
                            }
                        }
                    }
                }
                
            } catch {
                Utility.hideLoading()
                print(error.localizedDescription)
            }
        }
        
       // updateUI()
    }
    
    private func updateDistance (distance: String) {
        
        APIClient.shared.updateDistance(distance: distance, orderID: self.orderDetail.orderDetail.orderId) { (result, error, status) in
            
            if error == nil {
                print("updated location successfully")
                
            } else {
                
            }
        }
    }
    
    private func draw(src: [CLLocationCoordinate2D], dst: [CLLocationCoordinate2D], color:[UIColor]){
        mapsView.clear()
        let marker = GMSMarker()
        marker.position = src[0]
        marker.map = mapsView
       // marker.icon = #imageLiteral(resourceName: "ic_pin")
        
        for i in 0..<src.count {
            
            let origin = "\(src[i].latitude),\(src[i].longitude)"
            let destination = "\(dst[i].latitude),\(dst[i].longitude)"
            let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(kGoogleMapsAPI)"
                        
            let marker = GMSMarker()
            marker.icon = #imageLiteral(resourceName: "ic_pin")
            marker.position = dst[i]
            marker.map = mapsView
            Alamofire.request(url).responseJSON { response in
               // Utility.hideLoading()
                do {
                    let json = try JSON(data: response.data!)
                   // print(json)
                    let routes = json["routes"].arrayValue
                    
                    for route in routes {
                        let routeOverviewPolyline = route["overview_polyline"].dictionary
                        let points = routeOverviewPolyline?["points"]?.stringValue
                        let path = GMSPath.init(fromEncodedPath: points!)
                        let polyline = GMSPolyline(path: path)
                        polyline.strokeColor = color[i]
                        polyline.strokeWidth = 2
                        polyline.map = self.mapsView
                        self.isPolylineDrawn = true
                        
                    }
                    
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func showPulsator () {
        let pulsator = Pulsator()
        pulsator.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.6078431373, blue: 0.937254902, alpha: 1)
        pulsator.numPulse = 3
        pulsator.radius = 240.0
        marker.iconView?.transform = CGAffineTransform.init(translationX: 0, y: 200)
        marker.iconView?.cornerRadius = 100
        marker.iconView?.clipsToBounds = true
        marker.iconView?.layer.addSublayer(pulsator)
        pulsator.position = CGPoint(x: 100, y: 100)
        pulsator.start()
    }
    
    func createPath (location: CLLocation) {
        
        var source: [CLLocationCoordinate2D] = [CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude:  location.coordinate.longitude)]
        var destination: [CLLocationCoordinate2D] = []
        
        if self.orders != nil {
            
            for (index,order) in self.orders.orderDetailArray.enumerated() {
                
                let item = order.location.components(separatedBy:",")
                
                if destination.isEmpty {
                    destination.append(CLLocationCoordinate2D(latitude: Double(item[0])!, longitude: Double(item[1])!))
                    
                } else {
                    destination.append(CLLocationCoordinate2D(latitude: Double(item[0])!, longitude: Double(item[1])!))
                    source.append(destination[index - 1])
                }
            }
            
            colors.removeAll()
            for order in  self.orders.orderDetailArray {
                
                if order.status == OrderStatus.pending.rawValue || order.status == OrderStatus.readyToDeliver.rawValue {
                    colors.append(.blue)
                    
                } else if order.status == OrderStatus.inProgress.rawValue  {
                    colors.append(.green)
                    
                } else {
                    colors.append(.red)
                }
            }
            
            if !isPolylineDrawn {
                draw(src: source, dst: destination, color: colors)
            }
        }
    }
}

//MARK: - CLLocationManagerDelegate
extension MapsViewController: CLLocationManagerDelegate, GMSMapViewDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        guard status == .authorizedWhenInUse else {
            return
        }
        UserData.shared.locationManager.startUpdatingLocation()
        mapsView.isMyLocationEnabled = true
        mapsView.settings.myLocationButton = true
    }
    
    // 6
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.first else {
            return
        }
        
        if myLocation == nil {
            myLocation = location
        }
         
        long = location.coordinate.longitude
        lat = location.coordinate.latitude
        
        self.item = self.orderDetail.orderDetail.pickupLocation.components(separatedBy:",")
                
        if !isMapSetCurrentLocation {
            mapsView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            isMapSetCurrentLocation = true
        }
        
        marker.position = location.coordinate
       // self.showPulsator ()
        
        if item.first != "" {
             isReadyToSendLocation = false
        }
        
        if !drawPolygon {
            drawPathAndCalculateDistance()
        }
    }
}


//MARK: - OnJobViewDelegate
extension MapsViewController: OnJobViewDelegate {
    
    func cancelTapped() {
        //TODO: - Temp State manipulation replace with data from API
        if onJobView.cancelButton.titleLabel?.text == "Not Appeared" {
            let controller = NotAppearedNoteViewController()
            controller.delegate = self
            controller.modalPresentationStyle = .overCurrentContext
            self.present(controller, animated: true, completion: nil)
            return
        }
        let controller = CancelingNoteViewController()
        controller.delegate = self
        controller.modalPresentationStyle = .overCurrentContext
        self.present(controller, animated: true, completion: nil)
    }
    
    func checkDriverHasArrived () {
        
        if self.orderDetail.orderDetail.status == OrderStatus.arrived.rawValue {
            self.onJobView.cancelButton.setTitle("Not Appeared", for: .normal)
            
            if self.orders.type == "pickup" {
                self.onJobView.arriveButton.setTitle("Picked Up", for: .normal)
                
            } else {
                self.onJobView.arriveButton.setTitle("Delivered", for: .normal)
            }
            
        } else {
            self.onJobView.cancelButton.setTitle("Cancel Order", for: .normal)
            self.onJobView.arriveButton.setTitle("Arrived", for: .normal)
        }
    }

    func statusButtonTapped() {
        
        if self.onJobView.arriveButton.titleLabel?.text == "Picked Up" {

            if orderDetail.orderItems.count == 0 {
                self.showOkAlert("You can not place order with zero items.")
                return
            }

            StartOrder.setOrderPickedUp(orderId: self.orderDetail.orderDetail.orderId) { (result, error, status) in
               
                if error == nil  {
                    self.getCurrentList(driverId: UserData.shared.user.id)
                    
                } else {
                    self.showOkAlert(error?.localizedDescription ?? "")
                }
            }
            
        } else if self.onJobView.arriveButton.titleLabel?.text == "Delivered" {
            
            if !(self.collectedAmount >= 0) {
                viewOrderTapped()
                
                
            } else {
                
                if orderDetail.orderDetail.paymentStatus {
                    self.collectedAmount = orderDetail.orderDetail.amountPaid
                }
                
                StartOrder.setOrderDelivered(orderId: self.orderDetail.orderDetail.orderId, collectedAmount:  self.collectedAmount, status: OrderStatus.completed.rawValue) { (result, error, status) in
                    
                    if error == nil  {
                        self.getCurrentList(driverId: UserData.shared.user.id)
                        
                    } else {
                        self.showOkAlert(error?.localizedDescription ?? "")
                    }
                }
            }
            
        } else if self.onJobView.arriveButton.titleLabel?.text == "Arrived" {
            
            DriverArrivedData.setDriverArrived(orderId: self.orderDetail.orderDetail.orderId, customerId: self.orderDetail.orderDetail.customerId) {[weak self] (result, error, status) in
                
                if error == nil {
                    self?.orderDetail.orderDetail.status = OrderStatus.arrived.rawValue
                    
                    if self?.orders.type == "pickup" {
                        self?.onJobView.arriveButton.setTitle("Picked Up", for: .normal)

                    } else {
                        self?.onJobView.arriveButton.setTitle("Delivered", for: .normal)
                    }
                } else {
                    Utility.showAlertController(self!, error?.localizedDescription ?? "")
                }
            }
        }
    }
    
    func viewOrderTapped() {
        
        if orderDetail.orderDetail.status == OrderStatus.arrived.rawValue {
            let controller = NewJobNotificationViewController()
            controller.mapViewController = self
            controller.mapViewController = self
            controller.isComeFromViewOrderButton = false
            controller.order = self.orderDetail
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: true, completion: nil)
            
        } else {
            self.showOkAlert("You can not view order until you arrive to destined location.")
        }
    }
    
    func disableButton () {
        self.onJobView.cancelButton.alpha = 0.3
        self.onJobView.cancelButton.isUserInteractionEnabled = false
       
    }
}



//MARK: - CancelingNoteViewControllerDelegate
extension MapsViewController: CancelingNoteViewControllerDelegate {
    
    func okPressed(message: String = "Can not go there.") {
       
        CancelOrder.setCancelOrder(orderId: self.orderDetail.orderDetail.orderId, driver: UserData.shared.user.id, cancelNote: message) { (result, error, status) in
            
            if error == nil {
                self.getCurrentList(driverId: UserData.shared.user.id)
                
            } else {
                Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
}


//MARK: - NotAppearedNoteViewControllerDelegate
extension MapsViewController: NotAppearedNoteViewControllerDelegate {
    
    func cancelPressed() {
        
    }
    
    func okButtonPressed(message: String = "customer not appeared") {
        
        CancelOrder.setCancelOrder(orderId: self.orderDetail.orderDetail.orderId, driver: UserData.shared.user.id, cancelNote: message) { (result, error, status) in
            
            if error == nil {
                self.getCurrentList(driverId: UserData.shared.user.id)
                
            } else {
                 Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
}


//MARK: -
extension MapsViewController: CollectedAmountDelegate {
    
    func addCollectedAmount(amount: Double) {
        print(amount)
        self.collectedAmount = amount
        let item = self.orderDetail.orderDetail.pickupLocation.components(separatedBy:",")

        self.calculateTimeDistance(src: CLLocationCoordinate2D(latitude: self.myLocation?.coordinate.latitude ?? 0.0, longitude: self.myLocation?.coordinate.longitude ?? 0.0), dst: CLLocationCoordinate2D(latitude: Double(item[0])!, longitude: Double(item[1])!))
    }
    func paidStatus (status: Bool) {
        print(status)
    }
    
}


//MARK: -
extension MapsViewController: ConfirmDelegate {
    func confirmTapped() {
        onJobView.cancelButton.alpha = 0.3
        onJobView.cancelButton.isUserInteractionEnabled = false
    }
}
