//
//  OnJobView.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 5/13/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class OnJobView: UIView {

    //MARK: - Variables
    var view: UIView!
    weak var delegate: OnJobViewDelegate?
    
    //MARK: - Outlets
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var distanceLeftLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var arriveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var bottomDetailsView: UIView!
    
    
    //MARK: - View Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    //MARK: - View Setup
    func configure(order: OrderDetail) {
        orderIdLabel.text = "Order # \(order.orderId)"
        customerNameLabel.text = order.customerName
        addressLabel.text = order.pickupAddress
    }
    
    func setup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        setupView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }
    
    func setupView() {
        containerView.cornerRadius = 24
        containerView.addShadow(offset: CGSize(width: 0, height: 0), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.26), opacity: 1.0, radius: 0.5)
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    
    //MARK: - Actions
    @IBAction func cancelPressed(_ sender: Any) {
        delegate?.cancelTapped()
    }
    
    @IBAction func statusButtonPressed(_ sender: Any) {
        delegate?.statusButtonTapped()
    }
    
    @IBAction func viewOrderPressed(_ sender: Any) {
        delegate?.viewOrderTapped()
    }
}
