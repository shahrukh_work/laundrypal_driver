//
//  SignInViewController.swift
//  CleaningPal-Driver
//
//  Created by Mian Faizan Nasir on 4/22/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import FirebaseCrashlytics
import FirebaseAnalytics
import SwiftyGif
import AVFoundation
import AVKit

extension SignInViewController: SwiftyGifDelegate {
    func gifDidStop(sender: UIImageView) {
        logoAnimationView.isHidden = true
        showLoader = true
    }
}

class SignInViewController: UIViewController {
    
    
    //MARK: - Outlets
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var emailTextField: FloatingTextField!
    @IBOutlet weak var passwordTextField: FloatingTextField!
    @IBOutlet weak var signinButton: UIButton!
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var containreViewHeightConstraint: NSLayoutConstraint!
    
    
    let logoAnimationView = LogoAnimationView()
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer? = nil
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        if !showLoader {
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(playerItemDidReachEnd),
                                                   name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                   object: nil)
           
        }
        setupView()
        emailTextField.delegate = self
        passwordTextField.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        tap.numberOfTapsRequired = 3
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        if showLoader == false{
            loadVideo()
            showLoader = true
        }
        setupView()
        
        if let url = DataManager.shared.getBaseURL() {
            APIRoutes.baseUrl = url
            return
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    //MARK: - View Setup
    func setupView() {
        passwordTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        emailTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        
        passwordTextField.addTarget(self, action: #selector(self.textFieldDidEditing(_:)), for: .editingDidEnd)
        emailTextField.addTarget(self, action: #selector(self.textFieldDidEditing(_:)), for: .editingDidEnd)
        
        containerView.layer.cornerRadius = 17
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        let tapRecon = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        containerView.addGestureRecognizer(tapRecon)
    }
    
    
    //MARK: - Selector
    
    @objc func playerItemDidReachEnd(notification: NSNotification) {
        playerLayer?.zPosition = -1
    }

    @objc func doubleTapped() {
        
        // Create the alert controller
        let alertController = UIAlertController(title: "Set Base URL", message: "", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Staging", style: UIAlertAction.Style.default) {
            UIAlertAction in
            APIRoutes.baseUrl = "https://cleaningpal-backend-staging.codesorbit.com"
            DataManager.shared.setBaseURL(url: APIRoutes.baseUrl)
            NSLog("Live Pressed")
        }
        let cancelAction = UIAlertAction(title: "Live", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            APIRoutes.baseUrl = "https://cleaningpal-backend.codesorbit.com"
            DataManager.shared.setBaseURL(url: APIRoutes.baseUrl)
            NSLog("Staging Pressed")
        }
        
        
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        if containreViewHeightConstraint.constant != 220 {
            return
        }
        containreViewHeightConstraint.constant = Utility.getScreenHeight() - (logoView.frame.height + logoView.frame.origin.x - 20)
        emailTextField.isUserInteractionEnabled = true
        passwordTextField.isUserInteractionEnabled = true
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
            self.titleLabel.alpha = 1.0
            self.signinButton.alpha = 1.0
            self.emailTextField.becomeFirstResponder()
        }
    }

    
    @objc func textFieldDidChange(_ textField: FloatingTextField) {
        
        if textField == emailTextField {
            textField.floatTitleLabel(titleLabel: emailLabel)
            
        } else if textField == passwordTextField {
            textField.floatTitleLabel(titleLabel: passwordLabel)
        }
    }
    
    @objc func textFieldDidEditing(_ textField: FloatingTextField) {
        
        if emailTextField.text == "" && passwordTextField.text == "" {
            containreViewHeightConstraint.constant = 220
            emailTextField.isUserInteractionEnabled = false
            passwordTextField.isUserInteractionEnabled = false
            
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
                self.titleLabel.alpha = 0.0
                self.signinButton.alpha = 0.0
            }
        }
    }
    
    
    //MARK: - Actions
    @IBAction func signinPressed(_ sender: Any) {
        let email = emailTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        if email == "" || password == "" {
            self.showOkAlert("Email and password are required")
            return
        }
        
        if !Utility.isValidEmail(emailStr: email) {
            self.showOkAlert("Invalid Email")
            return
        }
        
        UserData.loginAuthentication(email:email, password:password) { (data, error) in
            
            
            if error == nil {
                
                if let data = data {
                    
                    if data.status == "ACTIVE" {
                        APIClient.shared.updateTokenMethod(token: kFcmToken ?? "") { (data, error, status) in
                            
                            if error == nil {
                                print("token updated")
                                
                            } else {
                                self.showOkAlert("Due to some error you'll not be able to receive notifications.")
                            }
                        }
                        Utility.setupHomeAsRootViewController()
                        
                    } else if data.status == "USER_DISABLED" || data.status == "PAUSED" {
                        self.showOkAlert("Your account has been disabled by admin.")
                        
                    }
                }
                
            } else {
                Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
    
    
    //MARK : - Methods
    
    private func loadVideo() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
        } catch { }
        
        let path = Bundle.main.path(forResource: "launch1", ofType:"mp4")
        
        player = AVPlayer(url: NSURL(fileURLWithPath: path!) as URL)
        playerLayer = AVPlayerLayer(player: player)
        
        playerLayer?.frame = self.view.frame
        playerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer?.zPosition = 1
        
        self.view.layer.addSublayer(playerLayer!)
        
        player?.seek(to: CMTime.zero)
        player?.play()
    }
    
}


//MARK: - UITextFieldDelagete
extension SignInViewController: UITextFieldDelegate {
    
    override func resignFirstResponder() -> Bool {
        super.resignFirstResponder()
        containreViewHeightConstraint.constant = Utility.getScreenHeight() + (logoView.frame.height + logoView.frame.origin.x - 20)
        emailTextField.isUserInteractionEnabled = false
        passwordTextField.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
            self.titleLabel.alpha = 0.0
            self.signinButton.alpha = 0.0
            self.emailTextField.resignFirstResponder()
        }
        return true
    }
}
