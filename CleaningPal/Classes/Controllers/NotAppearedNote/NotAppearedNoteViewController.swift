//
//  NotAppearedNoteViewController.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 6/9/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class NotAppearedNoteViewController: UIViewController {

    
    //MARK: - Variables
    weak var delegate: NotAppearedNoteViewControllerDelegate?
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    //MARK: - Actions
    @IBAction func okPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        delegate?.okButtonPressed(message: "Customer not appeared!")
    }
    
    
    @IBAction func cancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        delegate?.cancelPressed()
    }
}
