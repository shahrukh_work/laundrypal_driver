//
//  EasyOrderPopupViewController.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 6/9/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
enum ShowAlertMode {
    case easyOrder
    case alert
}

class EasyOrderPopupViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    
    //MARK: - Variables
    var alertType: ShowAlertMode = .easyOrder
    var descriptionText = "The minimum price for Easy Order is Rs 2000. \n The Driver will insert details upon Pickup. \n Please review the Price List."
    var titleText = "Easy way to Order:"
    
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        titleLabel.text = titleText
        descriptionLabel.text = descriptionText
        
        if alertType == .alert {
            okButton.setTitle("OK", for: .normal)
            cancelButton.isHidden = true
        }
    }
    
    //MARK: - Actions
    @IBAction func okPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func cancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
