//
//  SideMenuViewController.swift
//  CleaningPal-Driver
//
//  Created by Mian Faizan Nasir on 4/27/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
    
    
    //MARK: - Variables
    var homeController: UINavigationController!
    
    
    //MARK: - Outlets
    @IBOutlet weak var sideViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var shadowView: UIView!
    
    
    //MARK: - Variables
    let optionIcons: [UIImage] = [#imageLiteral(resourceName: "home"), #imageLiteral(resourceName: "question"), #imageLiteral(resourceName: "help"), #imageLiteral(resourceName: "settings")]
    let options = ["Home", "Help", "FAQs", "Settings"]
    
    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !(self.slideMenuController()?.isLeftHidden() ?? false) {
            self.shadowView.alpha = 1.0
        }
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        shadowView.layer.cornerRadius = 11
        shadowView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner]
        tableView.delegate = self
        tableView.dataSource = self
        
        containerView.cornerRadius = 24
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        tableView.cornerRadius = 24
        tableView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        let gestureRecon = UITapGestureRecognizer(target: self, action: #selector(shadowViewTapped))
        shadowView.isUserInteractionEnabled = true
        shadowView.addGestureRecognizer(gestureRecon)
    }
    
    
    //MARK: - Selectors
    @objc func shadowViewTapped() {
        closeLeft()
    }
    
    
    //MARK: - Actions
    @IBAction func logoutButtonTapped(_ sender: Any) {
        Utility.showLoading()
        
        APIClient.shared.logoutMethod { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                DataManager.shared.deleteUser()
                Utility.loginRootViewController()
                
                
            } else {
                //TODO: Alert
            }
        }
        
    }
    
    @IBAction func hamburgerPressed(_ sender: Any) {
        closeLeft()
    }
    
    @IBAction func bellButtonTapped(_ sender: Any) {
        
        let controller = NotifiationsViewController()
        controller.modalPresentationStyle = .overFullScreen
        self.present(controller, animated: true, completion: nil)
        
        closeLeft()
    }
}


//MARK: - TableView DataSource & Delegate
extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(SideMenuTableViewCell.self, indexPath: indexPath)
        cell.selectionStyle = .none
        cell.configCell(image: optionIcons[indexPath.row], option: options[indexPath.row], itemIndex: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch(indexPath.row) {
            
        case 0:
            closeLeft()
            
        case 1:
            homeController.pushViewController(SupportCenterChatViewController(), animated: true)
            closeLeft()
            
        case 2:
            homeController.pushViewController(FAQsViewController(), animated: true)
            closeLeft()
            
        case 3:
            homeController.pushViewController(SettingsViewController(), animated: true)
            closeLeft()
            
        default:
            return
        }
    }
}


//MARK: - SideMenuDelegate
extension SideMenuViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.shadowView.alpha = 1.0
            self.sideViewWidthConstraint.constant = 33
            self.view.layoutIfNeeded()
        })
    }
    
    func leftDidOpen() {
        self.shadowView.alpha = 1.0
        self.sideViewWidthConstraint.constant = 33
    }
    
    func leftWillClose() {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.sideViewWidthConstraint.constant = 220
            self.shadowView.alpha = 0.0
            self.view.layoutIfNeeded()
        })
    }
    
    func leftDidClose() {
        self.shadowView.alpha = 0.0
        self.sideViewWidthConstraint.constant = 220
    }
}

