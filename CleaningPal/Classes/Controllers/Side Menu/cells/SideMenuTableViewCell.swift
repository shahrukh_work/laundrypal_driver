//
//  SideMenuTableViewCell.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/27/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    
    //MARK: - Outlets
    @IBOutlet weak var optionLabel: UILabel!
    @IBOutlet weak var optionImageView: UIImageView!
    
    
    //MARK: - View  Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Methods
    func configCell(image: UIImage, option: String, itemIndex: Int) {
        optionImageView.image = image
        optionLabel.text = option
    }
}
