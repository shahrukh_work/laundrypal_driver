//
//  CancelingNoteViewController.swift
//  CleaningPal-Driver
//
//  Created by Mian Faizan Nasir on 4/23/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class CancelingNoteViewController: UIViewController {

    
    //MARK: - Variables
    weak var delegate: CancelingNoteViewControllerDelegate?
    
    
    //MARK: - Outlets
    @IBOutlet weak var canceNoteTextArea: UITextView!
    
    
    //MARK: - Viewcontroller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        canceNoteTextArea.delegate =  self
        canceNoteTextArea.delegate = self
        canceNoteTextArea.text = "Please type cancel note here..."
        canceNoteTextArea.textColor = UIColor.lightGray
    }
    
    
    //MARK: - Actions
    @IBAction func okPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
        if canceNoteTextArea.text != "Please type cancel note here..." {
            delegate?.okPressed(message: canceNoteTextArea.text ?? "")
        }
    }
        
}

//MARK: - UITextViewDelegate
extension CancelingNoteViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if canceNoteTextArea.textColor == UIColor.lightGray {
            canceNoteTextArea.text = ""
            canceNoteTextArea.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {

        if canceNoteTextArea.text == "" {
            canceNoteTextArea.text = "Please type cancel note here..."
            canceNoteTextArea.textColor = UIColor.lightGray
        }
    }
}
