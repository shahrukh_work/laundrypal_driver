//
//  CollectedAmountViewController.swift
//  CleaningPal-Driver
//
//  Created by Mian Faizan Nasir on 4/22/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class CollectedAmountViewController: UIViewController {
    
    //MARK: - Variables
    var collectedAmount: ((_ amount: Double) -> ())?
    var addPressed: (() -> ())?
    var typeOfOrder = "pickup"
    var orderDetailVC: NewJobNotificationViewController?
    var price = ""
    var orderId = -1
    var customerId = -1
    
    //MARK: - Outlets
    @IBOutlet weak var amountTextField: UITextField!
    weak var delegate: CollectedAmountDelegate?
    weak var paidDelegate : PaidStatusDelegate?
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    //MARK: - View Setup
    func setupView() {
    }
    
    
    //MARK: - Actions
    @IBAction func cancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    
    @IBAction func addPressed(_ sender: Any) {
        
        if (amountTextField.text ?? "") == "" {
            self.showOkAlert("Please enter amount")
            return
        }
        if self.typeOfOrder != "Pickup" {
            
          self.dismiss(animated: true, completion: {
            self.addPressed?()
          })
            
        } else {
            self.dismiss(animated: true, completion: nil)
        }
        
        //calculaptePayment()
    }
    
    @IBAction func amountChanged(_ sender: Any) {
        var price = "0"
        var text = amountTextField.text ?? "0.0"
        price = text
        if text == "0.0" || text == "" {
            amountTextField.text = ""
            return
        }
        
        if text.contains("PKR ") {
            price = text.components(separatedBy: " ")[1]
            if price == "" {
                price = "0"
            }
           // _ = text.remove(at: text.startIndex)
        }
        
        if text.isEmpty {
            text = "0"
        }
        
        if price == "0" {
            amountTextField.text = "PKR " + price
            return 
        }
        self.price = price
        amountTextField.text = "PKR " + price
        collectedAmount?(Double(price)!)
        delegate?.addCollectedAmount(amount: Double(price)!)
        delegate?.paidStatus(status: false)
    }
    
    
    //MARK: - private method
    func calculaptePayment () {
        Utility.showLoading()
        
        APIClient.shared.calculatePaymentMethod(orderId: orderId, customerId: customerId, receivedAmount: price) { (result, error, status) in
            
            Utility.hideLoading()
            if error == nil {
                
              
            } else {
                print(error?.localizedDescription)
                self.showOkAlert(error?.localizedDescription ?? "")
            }
        }
    }
}


