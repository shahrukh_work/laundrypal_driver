//
//  NotificationsTableViewCell.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/27/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class NotificationsTableViewCell: UITableViewCell {

    
    //MARK: - Outlets
    @IBOutlet weak var dashView: UIView!
    @IBOutlet weak var orderStatusLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var newNotificationIndicatorView: UIView!
    @IBOutlet weak var addressLabel2: UILabel!
    
    
    //MARK: - Variables
    var isCellForLocation = true
    
    
    //MARK: - View  Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        Utility.showDashLine(view: dashView, color: .darkGray, height: 12)
    }
    
    
    //MARK: - Methods
    func configCell(notification: NotificationList) {
        orderStatusLabel.text = notification.message
        addressLabel.text = notification.startLocation == "" ? "No address" : notification.startLocation
        addressLabel2.text = notification.endLocation == "" ? "No address" : notification.endLocation
        orderIdLabel.text = "Job Id : \(notification.batchId)"
        orderStatusLabel.text = "Type : \(notification.type == "" ? "No Type" : notification.type)"
    }
}
