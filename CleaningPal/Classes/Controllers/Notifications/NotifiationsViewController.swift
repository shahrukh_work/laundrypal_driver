//
//  NotifiationsViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/27/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper

class NotifiationsViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    
    //MARK: - Variables
    var notifications = Mapper<NotificationListData>().map(JSON: [:])!
    
    
    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        getNotificationList ()
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        containerView.cornerRadius = 24
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        tableView.cornerRadius = 24
        tableView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
    }
    
    
    //MARK: - Actions
    @IBAction func clearButtonTapped(_ sender: Any) {
        clearNotification()
    }
    
    @IBAction func closeTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: - Methods
    func getNotificationList () {
        
        NotificationListData.notificationList(limit: 10) {[weak self] (result, error, status) in
            
            if error == nil {
                
                self?.notifications = result!
                self?.tableView.reloadData()
                
            } else {
                Utility.showAlertController(self!, error?.localizedDescription ?? "")
            }
        }
    }
    
    private func clearNotification () {
        Utility.showLoading()

        
        APIClient.shared.clearNotificationMethod {[weak self] (result, error, status) in
            Utility.hideLoading()
            
            if error != nil {
                Utility.showAlertController(self!, "Error occured while clearing notifications")
                
            } else {
                self?.notifications.notificationList.removeAll()
                self?.tableView.reloadData()
            }
        }
    }
}


//MARK: - TableView Delegate & DataSource
extension NotifiationsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        notifications.notificationList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(NotificationsTableViewCell.self, indexPath: indexPath)
        cell.configCell(notification: notifications.notificationList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //TODO: - tableView cell selection
    }
}
