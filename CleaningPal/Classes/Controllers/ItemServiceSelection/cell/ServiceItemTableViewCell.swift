//
//  ServiceItemTableViewCell.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 5/14/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class ServiceItemTableViewCell: UITableViewCell {

    
    //MARK: - Outlets
    @IBOutlet weak var selectionButton: UIButton!
    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var itemPriceLabel: UILabel!
    
    
    //MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Public Methods
    func config(data: WashingMethods) {
        itemTitleLabel.text = data.washingMethodName
        itemPriceLabel.text = "\(data.price)"
        
        if data.isSelected {
            selectionButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            
        } else {
            selectionButton.setImage(#imageLiteral(resourceName: "unchecked"), for: .normal)
        }
    }
}
