//
//  ItemServiceSelectionViewController.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 5/14/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper

class ItemServiceSelectionViewController: UIViewController {
    
    
    //MARK: - Variables
    var count = 1
    var item = Mapper<ListItemsData>().map(JSON: [:])!
    var dataSource = [WashingMethods]()
    var subCategoryName = ""
    var selectedItem = Mapper<WashingMethods>().map(JSON: [:])!
    weak var delegate:ItemServiceSelectionViewControllerDelegate?
    var itemIndex = -1
    
    //MARK: - Outlets
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var itemNameLabel: UILabel!
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    func setupView() {
        tableView.dataSource = self
        tableView.delegate = self
        loadData()
        
        itemIndex = item.itemWashingMethodArray.firstIndex(where: { (itemWash) -> Bool in
            return itemWash.isSelected
        }) ?? 0
    }
    
    
    //MARK: - Actions
    @IBAction func decreaseCount(_ sender: Any) {
        
        if count > 1 {
            count-=1
            countLabel.text = "\(count)"
        }
        selectedItem.qauntity = count
        updatePrice()
    }
    
    @IBAction func increaseCount(_ sender: Any) {
        count+=1
        countLabel.text = "\(count)"
        selectedItem.qauntity = count
        updatePrice()
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.selectedItem.subCategoryName = subCategoryName
        self.selectedItem.itemId = item.id
        self.selectedItem.isLocaleAdded = true
        
        let orderItem =  Mapper<OrderItems>().map(JSON: [:])!
        orderItem.name = item.itemName
        orderItem.price = item.itemWashingMethodArray[itemIndex].price * count
        orderItem.quantity = count
        orderItem.subcategoryName = item.subCategoryName
        orderItem.itemId = item.id
        orderItem.isLocaleAdded = true
        orderItem.washingMethod = item.itemWashingMethodArray[itemIndex].washingMethodName
        
        delegate?.addPressed(data: orderItem)
    }
    
    @IBAction func crossPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: - Private Methods
    private func loadData() {
        updatePrice()
        itemNameLabel.text = item.itemName
        self.tableView.reloadData()
    }
    
    private func updatePrice() {
        let selectedItem = dataSource.first { (item) -> Bool in
            return item.isSelected
        }
        
        if let item = selectedItem {
            let price = count * (Int(item.price))
            priceLabel.text = "\(price)"
            
        } else {
            priceLabel.text = "0"
        }
    }
}


//MARK: - TableView DataSoure & Delegate
extension ItemServiceSelectionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(ServiceItemTableViewCell.self, indexPath: indexPath)
        cell.config(data: dataSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        dataSource.forEach { (item) in
            item.isSelected = false
        }
        self.itemIndex = indexPath.row
        self.selectedItem = dataSource[indexPath.row]
        dataSource[indexPath.row].isSelected = true
        tableView.reloadData()
        updatePrice()
    }
    
}
