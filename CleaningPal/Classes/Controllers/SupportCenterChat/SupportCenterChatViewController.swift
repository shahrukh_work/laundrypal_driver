//
//  SupportCenterChatViewController.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 5/14/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SocketIO
import ObjectMapper
import SDWebImage

class SupportCenterChatViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var bottomMessageView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var sendMessageViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var messageTextViewHeightConstraint: NSLayoutConstraint!
    
    
    //MARK: - Variables
    var admin = Mapper<AdminData>().map(JSON: [:])!
    var chatData = Mapper<ChatData>().map(JSON: [:])!
    var adminImage = UIImageView()
    var isChatActive = false
    var message = ""
    
    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        Utility.socketImplementation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        registerKeyboardNotifications()
        SlideMenuOptions.panFromBezel = false
        dataSource ()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SlideMenuOptions.panFromBezel = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - View Setup
    private func setupView() {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        bottomMessageView.layer.cornerRadius = bottomMessageView.frame.height/2
        tableView.delegate = self
        tableView.dataSource = self
        messageTextView.delegate = self
        messageTextView.text = "Type"
        messageTextView.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        
        if tableView.numberOfRows(inSection: 0) != 0 {
            tableView.scrollToRow(at: IndexPath(row: tableView.numberOfRows(inSection: 0) - 1, section: 0), at: .bottom, animated: true)
        }
        tableView.cornerRadius = 24
        tableView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        let doneToolBar = UIToolbar()
        doneToolBar.sizeToFit()
        let spaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let btnDone = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(keyboardDonePressed))
        doneToolBar.items = [spaceItem, btnDone]
        messageTextView.inputAccessoryView = doneToolBar
    }
    
    
    //MARK: - DataSource
    func dataSource () {
        getChatHistory ()
      //  getChatTiming ()//
        self.socketsListiner ()
    }
        
    
    //MARK: - Selectors
    @objc func keyboardDonePressed() {
        self.view.endEditing(true)
    }
    
    
    //MARK: - Actions
    @IBAction func sendButonPressed(_ sender: Any) {
        
        if messageTextView.text != "" {
            let chatObj = Mapper<ChatHistory>().map(JSON: [:])!
            chatObj.adminId = admin.id
            chatObj.driverId = UserData.shared.user.id
            chatObj.message = messageTextView.text ?? ""
            chatObj.sendBy = 2
            chatObj.adminImage = admin.image
            chatObj.driverImage = UserData.shared.user.image
            chatData.chatHistory.append(chatObj)
            UserData.shared.socket?.emit("support-chat", ["driver_id" : UserData.shared.user.id, "admin_id" : admin.id, "message" :messageTextView.text ?? "","send_by":"2","type":"driver"])
                   
            messageTextView.text = ""
            reloadTable()
        }
        messageTextViewHeightConstraint.constant = 37
       
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func notificationPressed(_ sender: Any) {
        let controller = NotifiationsViewController()
        controller.modalPresentationStyle = .overFullScreen
        self.present(controller, animated: true, completion: nil)
        
        closeLeft()
    }
    
    
    //MARK: - Methods
    func reloadTable() {
        tableView.reloadData()
        if tableView.numberOfRows(inSection: 0) - 1 > 0 {
            tableView.scrollToRow(at: IndexPath(row: tableView.numberOfRows(inSection: 0) - 1, section: 0), at: .bottom, animated: true)
        }
    }
    
// TODO: - Schedule Chat Commented Function

//    func getChatTiming () {
//
//        ChatTimingData.getChatTiming { (result, error, status) in
//
//            if error == nil {
//                self.getChatHistory ()
//                self.isChatActive = result?.status ?? false
//                self.message = result?.message ?? ""
//            }
//        }
//    }
    
    func getOnlineAdmin () {
        
        AdminData.getOnlineAdmin {[weak self] (result, error, status) in
            
            if error == nil {
                if result?.id ?? -1 == -1 {
                    Utility.showAlertController(self!, "No admin available right now. Kindly try again after sometime.")
                    return
                }
                self?.admin = result!
                
            } else {
                Utility.showAlertController(self!, error?.localizedDescription ?? "")
            }
        }
    }
    
    func getChatHistory () {
        
        ChatData.getChatHistory {[weak self] (result, error, status) in
            
            if error == nil {
                
                self?.chatData = result!
                self?.chatData.chatHistory = (result?.chatHistory.reversed())!
                
// TODO: - Schedule Chat Commented
               // if !(self?.isChatActive ?? false) {
//                    self?.messageTextView.isUserInteractionEnabled = false
//                    let chatObj = Mapper<ChatHistory>().map(JSON: [:])!
//                    chatObj.message = self?.message ?? ""
//                    chatObj.sendBy = 0
//                    self?.chatData.chatHistory.append(chatObj)
//
//               } else {
                    self?.messageTextView.isUserInteractionEnabled = true
                    self?.getOnlineAdmin ()
//                }
                self?.reloadTable()
                
            } else {
                Utility.showAlertController(self!, error?.localizedDescription ?? "")
            }
        }
    }
    
    func socketsListiner () {
        
        UserData.shared.socket?.on("chat-message") {[weak self] (sockData, ackEmitter) in
            
            if let socketData = sockData.first {
                
                if let data = Mapper<ChatHistory>().map(JSONObject: socketData) {
                    self?.chatData.chatHistory.append(data)
                    self?.reloadTable()
                    
                } else {
                    print("error parsing chat")
                }
            }
        }
    }
}


//MARK: - TableView Delegate & DataSource
extension SupportCenterChatViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 98
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.register(DateSectionHeaderTableViewCell.self, indexPath: IndexPath(row: 0, section: section ))
        return header
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatData.chatHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if chatData.chatHistory[indexPath.row].sendBy == 0 {
            let cell = tableView.register(IncomingMessageTableViewCell.self, indexPath: indexPath)
            cell.configCell(history: chatData.chatHistory[indexPath.row])
            return cell
            
        } else {
            let cell = tableView.register(OutgoingMessageTableViewCell.self, indexPath: indexPath)
            cell.configCell(history: chatData.chatHistory[indexPath.row])
            return cell
        }
    }
}


//MARK: - TextView Delegate
extension SupportCenterChatViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1) {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == "" {
            messageTextViewHeightConstraint.constant = 37
            textView.text = "Type"
            textView.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let minTextViewHeight: CGFloat = 37
        let maxTextViewHeight: CGFloat = 111
        
        var height = ceil(textView.contentSize.height) // ceil to avoid decimal
        
        if (height < minTextViewHeight) {
            height = minTextViewHeight
        }
        
        if (height > maxTextViewHeight) {
            height = maxTextViewHeight
        }
        
        if height != messageTextViewHeightConstraint.constant {
            messageTextViewHeightConstraint.constant = height
            textView.setContentOffset(.zero, animated: false) // scroll to top to avoid "wrong contentOffset" artefact when line count changes
        }
    }
}


//MARK: - KeyBoardNotifications
extension SupportCenterChatViewController {
    
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
                
        if keyboardSize.height > 200  && sendMessageViewBottomConstraint.constant < 100{
            let window = UIApplication.shared.keyWindow
            
            UIView.animate(withDuration: 0.1) {
                
                if let win = window {
                    self.sendMessageViewBottomConstraint.constant = self.sendMessageViewBottomConstraint.constant + keyboardSize.height -  win.safeAreaInsets.bottom
                }
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        
        if keyboardSize.height > 200  && sendMessageViewBottomConstraint.constant > 100 {
            
            UIView.animate(withDuration: 0.1) {
                self.sendMessageViewBottomConstraint.constant = 8
                self.view.layoutIfNeeded()
            }
        }
    }
}
