//
//  SettingsViewController.swift
//  CleaningPal-Driver
//
//  Created by Mian Faizan Nasir on 4/24/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var notificationsSwitch: UISwitch!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SlideMenuOptions.panFromBezel = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SlideMenuOptions.panFromBezel = true
    }
    
    
    //MARK: - View Setup
    func setupView() {
        containerView.cornerRadius = 24
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        scrollView.cornerRadius = 24
        scrollView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        contentView.cornerRadius = 24
        contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    
    //MARK: - Actions
    @IBAction func notificationSwitchValueChanged(_ sender: UISwitch) {
        self.setNotifitions(driverId: UserData.shared.user.id, status: sender.isOn)
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func bellButtonTapped(_ sender: Any) {
        let controller = NotifiationsViewController()
        controller.modalPresentationStyle = .overFullScreen
        self.present(controller, animated: true, completion: nil)
    }
    
    
    //MARK: - Methods
    func setNotifitions (driverId: Int, status: Bool) {
        
        NotificationData.setNotifications(driverId: driverId, status: status) { (data, error, message) in
            
            if error == nil {
                print(data)
                
            } else {
                Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
}
