//
//  CurrentJobTableViewCell.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 6/2/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class CurrentJobTableViewCell: UITableViewCell {
    
    
    //MARK: - Variables
    weak var delegate: CurrentJobTableViewCellDelegate?
    var indexPath =  IndexPath(row: 0, section: 0)
    
    
    //MARK: - Outlets
    @IBOutlet weak var orderCountLabel: UILabel!
    @IBOutlet weak var endAddressLabel: UILabel!
    @IBOutlet weak var dashView: UIView!
    @IBOutlet weak var redView: UIImageView!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var itemCountLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    //MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        Utility.showDashLine(view: dashView, color: .darkGray, height: 12)
    }
    
    
    //MARK: - Actions
    @IBAction func inProgressPressed(_ sender: Any) {
        delegate?.inProgressPressed(indexPath: self.indexPath)
    }
    
    
    //MARK: - Public Methods
    func config(data: Orders, indexPath: IndexPath) {
        self.indexPath = indexPath
        
        //TODO: - Temp Data Population replace with API data
        orderIdLabel.text = "\(data.id)"
        descriptionLabel.text = "No description"
        timeLabel.text = data.orderDetailArray.first?.availableStartTime ?? ""
        timeLabel.text = timeLabel.text! + "  " + (data.orderDetailArray.first?.pickupDate ?? "")
        addressLabel.text = data.orderDetailArray.first?.address ?? ""
        endAddressLabel.text = data.orderDetailArray.last?.address ?? ""
        orderCountLabel.text = "\(data.count)"
        priceLabel.text = "\(data.price)"
        typeLabel.text = "Type: \(data.type)"
    }
}
