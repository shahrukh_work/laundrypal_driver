//
//  JobsTableTableViewCell.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 5/13/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class JobsTableTableViewCell: UITableViewCell {
    
    
    //MARK: - Variables
    weak var delegate: JobsTableTableViewCellDelegate?
    var indexPath =  IndexPath(row: 0, section: 0)
    
    
    //MARK: - Outlets
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var checkImageViw: UIImageView!
    @IBOutlet weak var endLocationLabel: UILabel!
    @IBOutlet weak var dashView: UIView!
    
    
    //MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        Utility.showDashLine(view: dashView, color: .darkGray, height: 12)
    }
    
    
    //MARK: - Actions
    @IBAction func startPressed(_ sender: Any) {
        delegate?.startPressed(indexPath: self.indexPath)
    }
    
    
    //MARK: - Public Methods
    func config(data: Orders, indexPath: IndexPath, isCompleted: Bool) {
        self.indexPath = indexPath
        typeLabel.text = "Type: \(data.type)"
        orderIdLabel.text = "\(data.id)"
        timeLabel.text = "\(data.orderDetailArray.first?.availableStartTime ?? "")"
        addressLabel.text = "\(data.orderDetailArray.first?.address ?? "")"
        endLocationLabel.text = "\(data.orderDetailArray.last?.address ?? "")"
        
        if isCompleted {
            startButton.isUserInteractionEnabled = true
            startButton.isHidden = true
            checkImageViw.isHidden = false
            
        } else {
            startButton.isUserInteractionEnabled = true
            startButton.isHidden = false
            startButton.alpha = 1.0
            checkImageViw.isHidden = true
        }
    }
    
    func shouldShowStart(check: Bool) {
        
        if check {
            startButton.setTitle("Start", for: .normal)
            startButton.alpha = 1
            
        } else {
            startButton.setTitle("Pending", for: .normal)
            startButton.isUserInteractionEnabled = false
            startButton.alpha = 0.5
        }
    }
}
