//
//  HomeViewController.swift
//  CleaningPal-Driver
//
//  Created by Mian Faizan Nasir on 4/22/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import GoogleMaps
import ObjectMapper
import SocketIO
import SwiftyGif
import AVFoundation
import AVKit


extension HomeViewController: SwiftyGifDelegate {
    func gifDidStop(sender: UIImageView) {
        logoAnimationView.isHidden = true
        showLoader = true
    }
}

class HomeViewController: UIViewController {
    
    
    //MARK: - Variables
    var mode: HomeControllerMode = .current
    var runningOrders = Mapper<CurrentData>().map(JSON: [:])
    var pendingOrders = Mapper<PendingData>().map(JSON: [:])
    var completeOrders = Mapper<CompletedData>().map(JSON: [:])
    var locationString = ""
    var lat = 0.0
    var long = 0.0
    let logoAnimationView = LogoAnimationView()
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer? = nil
    
    
    //MARK: - Outlets
    @IBOutlet weak var completedValueLabel: UILabel!
    @IBOutlet weak var completedLabel: UILabel!
    @IBOutlet weak var completedButton: UIButton!
    @IBOutlet weak var pendingValueLabel: UILabel!
    @IBOutlet weak var pendingLabel: UILabel!
    @IBOutlet weak var pendingButton: UIButton!
    @IBOutlet weak var currentValueLabel: UILabel!
    @IBOutlet weak var currentLabel: UILabel!
    @IBOutlet weak var currentButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var mapsView: GMSMapView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var currentJobImageView: UIImageView!
    @IBOutlet weak var pendingJobImageView: UIImageView!
    @IBOutlet weak var completeJobImageView: UIImageView!
    @IBOutlet weak var notasksView: UIView!
    @IBOutlet weak var notaskLabel: UILabel!
    @IBOutlet weak var currentJobCountLabel: UILabel!
    @IBOutlet weak var pendingJobCountLabel: UILabel!
    @IBOutlet weak var completeJobCountLabel: UILabel!
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        if !showLoader {
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(playerItemDidReachEnd),
                                                   name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                   object: nil)
           
        }
        setupView()
        UserData.shared.manager = SocketManager(socketURL: URL(string: APIRoutes.baseUrl)!, config: [.log(true), .compress, .extraHeaders(["Authorization" : "Bearer \(UserData.shared.token)"])])
        Utility.socketImplementation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if showLoader == false{
            loadVideo()
            showLoader = true
        }
        getCurrentList(driverId: UserData.shared.user.id, isfirstTime: true)
        NotificationCenter.default.addObserver(
        self,
        selector: #selector(self.newPendingRecord),
        name: NSNotification.Name(rawValue: "Pending-Received"),
        object: nil)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        locationSetup()
        sendLocationToAdmin ()
        sendLocationToAdminSockets()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Pending-Received"), object: nil)
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        tableView.dataSource = self
        tableView.delegate = self
        
        if let styleURL = Bundle.main.url(forResource: "MapStyle", withExtension: "json") {
            
            do {
                mapsView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                
            } catch {
                print(error.localizedDescription)
            }
            
        } else {
            NSLog("Unable to find MapStyle.json")
        }
        
        containerView.cornerRadius = 24
        mapsView.cornerRadius = 24
        
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        mapsView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        updateView()
        dataSource ()
    }
    
    //MARK: - DataSource
    func dataSource () {
        getPendingList(driverId:UserData.shared.user.id , firstTimeLoad: true)
        getCompletedList(driverId: UserData.shared.user.id,firstTimeLoad: true)
        
    }
    
    @objc private func newPendingRecord(notification: NSNotification){
        getPendingList(driverId: UserData.shared.user.id)
    }
    
    
    //MARK: - Actions
    @objc func playerItemDidReachEnd(notification: NSNotification) {
        playerLayer?.zPosition = -1
    }
    
    @IBAction func currentPressed(_ sender: Any) {
        mode = .current
        getCurrentList(driverId: UserData.shared.user.id)
        
    }
    
    @IBAction func pendingPressed(_ sender: Any) {
        mode = .pending
        getPendingList(driverId: UserData.shared.user.id)
       
    }
    
    @IBAction func completePressed(_ sender: Any) {
        mode = .complete
        getCompletedList(driverId: UserData.shared.user.id)
        
    }
    
    @IBAction func hamburgerPressed(_ sender: Any) {
        openLeft()
    }
    
    @IBAction func bellButtonTapped(_ sender: Any) {
        let controller = NotifiationsViewController()
        controller.modalPresentationStyle = .overFullScreen
        self.present(controller, animated: true, completion: nil)
    }
    
    
    //MARK: - Private Methods
    private func loadVideo() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
        } catch { }
        
        let path = Bundle.main.path(forResource: "launch1", ofType:"mp4")
        
        player = AVPlayer(url: NSURL(fileURLWithPath: path!) as URL)
        playerLayer = AVPlayerLayer(player: player)
        
        playerLayer?.frame = self.view.frame
        playerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer?.zPosition = 1
        
        self.view.layer.addSublayer(playerLayer!)
        
        player?.seek(to: CMTime.zero)
        player?.play()
    }
    
    
    
    func sendLocationToAdmin () {
        Timer.scheduledTimer(withTimeInterval: 60, repeats: true) { timer in
            
            APIClient.shared.updateLocationMethod(location: self.locationString) { (result, error, status) in
                
                if error == nil {
                    print("location is emiting")
                }
            }
        }
    }
    
    func sendLocationToAdminSockets () {
        Timer.scheduledTimer(withTimeInterval: 6, repeats: true) { timer in
            
            UserData.shared.socket?.emit("share-location", ["driver_id" : UserData.shared.user.id, "latitude" : self.lat, "longitude" : self.long])
        }
    }
    
    private func locationSetup() {
        UserData.shared.locationManager.delegate = self
        
        switch(CLLocationManager.authorizationStatus()) {
            
        case .restricted, .denied:
            self.showOkAlertWithOKCompletionHandler("Application uses location data for navigations and maps. Kindly enable location services in order to use the application.") { ( _) in
                UIApplication.shared.open(NSURL(string:UIApplication.openSettingsURLString)! as URL)
            }
            
        case .authorizedAlways, .authorizedWhenInUse:
            UserData.shared.locationManager.startUpdatingLocation()
            UserData.shared.locationManager.startUpdatingHeading()
            
        case .notDetermined:
            UserData.shared.locationManager.requestWhenInUseAuthorization()
            
        default:
            break
        }
    }
    
    private func updateView() {
        
        switch mode {
            
        case .none:
            tableView.isHidden = true
            
        case .complete:
            tableView.isHidden = false
            selectedMode(.complete)
            
        case .pending:
            tableView.isHidden = false
            selectedMode(.pending)
            
        case .current:
            tableView.isHidden = false
            selectedMode(.current)
        }
        
        tableView.reloadData()
    }
    
    func selectedMode (_ mode: HomeControllerMode) {
        
        completedButton.alpha = 0.6
        completedLabel.alpha = 0.6
        completedValueLabel.alpha = 0.6
        
        currentButton.alpha = 0.6
        currentLabel.alpha = 0.6
        currentValueLabel.alpha = 0.6
        
        pendingButton.alpha = 0.6
        pendingLabel.alpha = 0.6
        pendingValueLabel.alpha = 0.6
        
        if mode == .complete {
            completedButton.alpha = 1.0
            completedLabel.alpha = 1.0
            completedValueLabel.alpha = 1.0
            
        } else if mode == .current {
            currentButton.alpha = 1.0
            currentLabel.alpha = 1.0
            currentValueLabel.alpha = 1.0
            
        } else if mode == .pending {
            pendingButton.alpha = 1.0
            pendingLabel.alpha = 1.0
            pendingValueLabel.alpha = 1.0
            
        }
    }
    
    
    //MARK: - Private Methods
    func getPendingList (driverId: Int, firstTimeLoad: Bool = false) {
        
        PendingOrder.loadPendingOrders(driverId: driverId, showLoading: showLoader) {[weak self] (data, error, message) in
            
            if error == nil {
                self?.pendingOrders = data
                
                if firstTimeLoad  {
                    self?.pendingJobCountLabel.text = "\(self?.pendingOrders?.orders.count ?? 0) jobs"
                    return
                }
                self?.pendingJobCountLabel.text = "\(self?.pendingOrders?.orders.count ?? 0) jobs"
                self?.updateView()
                self?.tableView.reloadData()
                
            } else {
                Utility.showAlertController(self!, error?.localizedDescription ?? "")
            }
        }
    }
    
    func getCompletedList (driverId: Int, firstTimeLoad: Bool = false) {
        
        CompletedOrder.loadCompletedOrders(driverId: driverId, showLoading: showLoader) {[weak self] (data, error, message) in
            
            if error == nil {
                self?.completeOrders = data
                self?.completeOrders?.orders.reverse()
                
                if firstTimeLoad  {
                    self?.completeJobCountLabel.text = "\(self?.completeOrders?.orders.count ?? 0) jobs"
                    return
                }
                self?.completeJobCountLabel.text = "\(self?.completeOrders?.orders.count ?? 0) jobs"
                self?.updateView()
                self?.tableView.reloadData()
                
            } else {
                Utility.showAlertController(self!, error?.localizedDescription ?? "")
            }
        }
    }
    
    func getCurrentList (driverId: Int, isfirstTime: Bool = false) {
        
        Utility.showLoading()
        CurrentOrder.loadCurrentOrders(driverId: driverId, showLoading: showLoader) {[weak self] (data, error, message) in
            Utility.hideLoading()
            
            if error == nil {
                self?.runningOrders = data
                self?.currentJobCountLabel.text = "\(self?.runningOrders?.orders.count ?? 0) jobs"
                self?.updateView()
                self?.tableView.reloadData()
                
            } else {
                Utility.showAlertController(self!, error?.localizedDescription ?? "")
            }
        }
    }
    
    func startJob (jobId: Int, row: Int) {
        
        self.pendingOrders?.orders.remove(at: row)
        self.pendingJobCountLabel.text = "\(self.pendingOrders?.orders.count ?? 0) jobs"
        self.tableView.reloadData()
        self.mode = .current
        self.updateView()
        
        StartJob.setStartJob(jobId: jobId) {[weak self] (result, error, status) in

            if error == nil {
                self?.getCurrentList(driverId: UserData.shared.user.id)

            } else {
                Utility.showAlertController(self!, error?.localizedDescription ?? "")
            }
        }
    }
}


//MARK: - TableView Datasource & Delegate
extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch mode {
            
        case .none:
            notasksView.isHidden = true
            return 0
            
        case .complete:
            
            if completeOrders?.orders.isEmpty ?? false {
                notaskLabel.text = "No Complete Tasks"
                notasksView.isHidden = false
                
            } else {
                notasksView.isHidden = true
            }
            return completeOrders?.orders.count ?? 0
            
        case .pending:
            
            if pendingOrders?.orders.isEmpty ?? false {
                notaskLabel.text = "No Pending Tasks"
                notasksView.isHidden = false
                
            } else {
                notasksView.isHidden = true
            }
            return pendingOrders?.orders.count ?? 0
            
        case .current:
            
            if runningOrders?.orders.isEmpty ?? false {
                notaskLabel.text = "No Running Tasks"
                notasksView.isHidden = false
                
            } else {
                notasksView.isHidden = true
            }
            return runningOrders?.orders.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if mode == .current {
            
            if runningOrders?.orders.count ?? 0 > 0 {
                
                let cell = tableView.register(CurrentJobTableViewCell.self, indexPath: indexPath)
                cell.config(data: (runningOrders?.orders[indexPath.row])!, indexPath: indexPath)
                cell.delegate = self
                return cell
            }
        }
        
        let cell = tableView.register(JobsTableTableViewCell.self, indexPath: indexPath)
        
        if mode == .complete {
            
            if self.completeOrders?.orders.count ?? 0 > 0 {
                cell.config(data: (self.completeOrders?.orders[indexPath.row])!, indexPath: indexPath, isCompleted: true)
            }
            
        } else {
            
            if self.pendingOrders?.orders.count ?? 0 > 0 {
                cell.config(data: (self.pendingOrders?.orders[indexPath.row])!, indexPath: indexPath, isCompleted: false)
                cell.shouldShowStart(check: runningOrders?.orders.isEmpty ?? false)
            }
        }
        cell.delegate = self
        return cell
    }
}


//MARK: - CLLocationManagerDelegate
extension HomeViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        guard status == .authorizedWhenInUse else {
            return
        }
        UserData.shared.locationManager.startUpdatingLocation()
        mapsView.isMyLocationEnabled = true
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.first else {
            return
        }
        locationString = "\(location.coordinate.latitude), \(location.coordinate.longitude)"
        lat = location.coordinate.latitude
        long = location.coordinate.longitude
        mapsView.isMyLocationEnabled = true
        mapsView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
    }
}


//MARK: - JobsTableTableViewCellDelegate
extension HomeViewController: JobsTableTableViewCellDelegate {
    
    func startPressed(indexPath: IndexPath) {
        startJob(jobId: (pendingOrders?.orders[indexPath.row].id)!, row: indexPath.row)
    }
}


//MARK: - CurrentJobTableViewCellDelegate
extension HomeViewController: CurrentJobTableViewCellDelegate {
    
    func inProgressPressed(indexPath: IndexPath) {
        
        if self.runningOrders?.orders[indexPath.row].orderDetailArray.count ?? 0 > 0 {
            let controller = MapsViewController()
            controller.jobId = self.runningOrders?.orders[indexPath.row].id ?? -1
            controller.orders = self.runningOrders?.orders[indexPath.row]
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
