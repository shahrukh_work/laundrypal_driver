//
//  OrderFromTableViewCell.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 5/15/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class OrderFromTableViewCell: UITableViewCell {

    
    //MARK: - Outlets
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var itemCountLabel: UILabel!
    @IBOutlet weak var orderIdLabel: UILabel!
    
    
    //MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Public Methods
    func config(data: OrderDetailData) {
        customerNameLabel.text = data.orderDetail.customerName
        itemCountLabel.text =  "\(data.orderItems.count)"
        orderIdLabel.text = "Order #\(data.orderDetail.orderId)"
    }
}
