//
//  OrderItemTableViewCell.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 5/15/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class OrderItemTableViewCell: UITableViewCell {

    
    //MARK: - Outlets
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemDescriptionLabel: UILabel!
    @IBOutlet weak var itemCountLabel: UILabel!
    @IBOutlet weak var itemPriceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(data: OrderItems) {
        itemNameLabel.text = data.name
     //   itemDescriptionLabel.text = "( \(data) )"
        itemCountLabel.text = "\(data.quantity)"
        itemPriceLabel.text = "\(data.price)"
    }
}
