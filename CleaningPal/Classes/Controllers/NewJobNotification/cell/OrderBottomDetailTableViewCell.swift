//
//  OrderBottomDetailTableViewCell.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 5/15/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class OrderBottomDetailTableViewCell: UITableViewCell {
    
    
    //MARK: - Variables
    weak var delegate: OrderBottomDetailTableViewCellDelegate?
    weak var confirmDelegate: ConfirmDelegate?
   

    
    //MARK: - Outlets
    @IBOutlet weak var starchTextView: UITextView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var starchView: UIView!
    @IBOutlet weak var discountedPrice: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var addItemButton: UIButton!
    @IBOutlet weak var addAmountButton: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var textViewAreaView: UIView!
    @IBOutlet weak var starchButton: UIButton!
    
    
    //MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        starchTextView.text = "Starch description"
        starchTextView.textColor = UIColor.lightGray
        starchTextView.delegate = self
    }
    
    
    //MARK: - Public Methods
    func config(data: OrderDetailData, isCameFromViewOrderButton: Bool, amountCollected: Double, isStarchApplied: Bool,mapVc: MapsViewController) {
        confirmDelegate = mapVc
        descriptionLabel.text = data.orderDetail.description == "" ? "No description" : data.orderDetail.description
        discountedPrice.text = "\(data.orderDetail.discountedPrice)"
        timeLabel.text = Utility.changeDateFormate(dataInString: data.orderDetail.pickupDate)
        addressLabel.text = data.orderDetail.pickupAddress
        confirmButton.isHidden = false
        discountedPrice.text = "\(data.orderDetail.discountedPrice)"
        totalPriceLabel.text = "\(data.orderDetail.totalPrice)"
        
        var gst = 0.0
        
        gst = Double((data.orderDetail.gst/100))*Double(data.orderDetail.totalPrice)
            totalPriceLabel.text = "\(Double(data.orderDetail.totalPrice)+gst)"
        confirmButton.isHidden = isCameFromViewOrderButton

        if data.orderDetail.orderType.lowercased() == "delivery" {
            
            if data.orderDetail.paymentStatus {
                addAmountButton.alpha = 1.0
                addAmountButton.isUserInteractionEnabled = true
                
            } else {
                addAmountButton.alpha = 1.0
                addAmountButton.isUserInteractionEnabled = true
            }
            
            if !(amountCollected > 0.0) {
                confirmButton.alpha = 0.5
                confirmButton.isUserInteractionEnabled = false
                
            } else {
                confirmButton.alpha = 1.0
                confirmButton.isUserInteractionEnabled = true
            }
            addItemButton.alpha = 0.5
            addItemButton.isUserInteractionEnabled = false

        } else {
            addItemButton.alpha = 1
            addItemButton.isUserInteractionEnabled = true
        }
        starchButton.setImage(isStarchApplied ? #imageLiteral(resourceName: "circle_filled") : #imageLiteral(resourceName: "circle_unfilled"), for: .normal)
        textViewAreaView.isHidden = !isStarchApplied
        confirmButton.isUserInteractionEnabled = !data.orderItems.isEmpty
        confirmButton.alpha = data.orderItems.isEmpty ? 0.5 : 1
    }
    
    @IBAction func addItemPressed(_ sender: Any) {
        delegate?.addItemPressed()
    }
    @IBAction func addAmountPressed(_ sender: Any) {

        delegate?.addCollectedAmountPressed()
    }
    @IBAction func confirmPressed(_ sender: Any) {
        confirmDelegate?.confirmTapped()
        delegate?.confirmPressed(startDescription: starchTextView.text == "Starch description" ? "" : starchTextView.text )
    }
    
    @IBAction func starchPressed(_ sender: Any) {
        delegate?.starchTapped()
    }
}


//MARK: - UITextViewDelegate
extension OrderBottomDetailTableViewCell: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Starch description"
            textView.textColor = UIColor.lightGray
        }
    }
}

