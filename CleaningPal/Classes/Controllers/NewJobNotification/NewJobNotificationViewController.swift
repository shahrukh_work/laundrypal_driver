//
//  NewJobNotificationViewController.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 5/15/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper

class NewJobNotificationViewController: UIViewController {
    
    
    //MARK: - Variables
    var order: OrderDetailData!
    var isComeFromViewOrderButton = false
    var mapViewController: MapsViewController?
    var collectedAmount = 0.0
    var navController: UINavigationController?
    var requestItem: [RequestItems] = []
    var isStarchApplied = false
    var isOrderConfirmed = false
    var paidStatus = false
    
    
    //MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dataSource()
    }
    
    //MARK: - View Setup
    func setupView() {
        tableView.dataSource = self
        tableView.delegate = self
        
        containerView.cornerRadius = 24
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        tableView.cornerRadius = 24
        tableView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
    }
    
    //MARK: - DataSource
    func dataSource () {
        
        for item in UserData.shared.totalItemsSelected {
            
            let tempItem = order.orderItems.first { (orderItem) -> Bool in
                return orderItem.itemId == item.itemId && orderItem.washingMethod == item.washingMethod
            }
            
            if let tempItem = tempItem {
                tempItem.quantity = tempItem.quantity + item.quantity
                tempItem.price = tempItem.price + item.price
                
            } else {
                order.orderItems.append(item)
            }
        }
        
        for item in order.orderItems {
            
            if item.isLocaleAdded {
                order.orderDetail.totalPrice = Double(item.price)
                order.orderDetail.totalQuantity = order.orderDetail.totalQuantity + item.quantity
            }
        }
        self.getCalcualtedPrice()
        UserData.shared.totalItemsSelected.removeAll()
    }
    
    func getCalcualtedPrice () {
        var request: [String: Any] = [:]
        request["order_id"] = self.order.orderDetail.orderId
        request["customer_id"] = self.order.orderDetail.customerId
        var items: [[String: Any]] = []
        
        for item in order.orderItems {
            var newItem: [String: Any] = [:]
            newItem["item_id"] = item.itemId
            newItem["price"] = item.price
            newItem["washing_method"] = item.washingMethod
            newItem["quantity"] = item.quantity
            items.append(newItem)
        }
        request["items"] = items
        
        PriceCalculation.getCalculatedPrice(request: request) { (result, error, status) in
            
            if error == nil {
                UserData.shared.totalPriceForEasyOrder = result?.totalPrice ?? 0.0
                self.order.orderDetail.totalPrice = result?.totalPrice ?? 0.0
                self.order.orderDetail.discountedPrice = result?.discountedPrice ?? 0.0
                self.order.orderDetail.totalQuantity = Int(result?.totalCount ?? "0")!
                self.tableView.reloadData()
                
            } else {
                //Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
    
    //MARK: - Actions
    @IBAction func bellButtonTapped(_ sender: Any) {
        let controller = NotifiationsViewController()
        controller.modalPresentationStyle = .overFullScreen
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: - Methods
    
}


//MARK: - TableView Delegate & DataSource
extension NewJobNotificationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return self.order.orderItems.count
        case 3:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
            
        case 0:
            let cell = tableView.register(OrderFromTableViewCell.self, indexPath: indexPath)
            cell.config(data: self.order)
            cell.selectionStyle = .none
            return cell
            
        case 1:
            let cell = tableView.register(OrderHeaderTableViewCell.self, indexPath: indexPath)
            cell.selectionStyle = .none
            return cell
            
        case 2:
            let cell = tableView.register(OrderItemTableViewCell.self, indexPath: indexPath)
            cell.configure(data: self.order.orderItems[indexPath.row])
            cell.selectionStyle = .none
            return cell
            
        case 3:
            let cell = tableView.register(OrderBottomDetailTableViewCell.self, indexPath: indexPath )
            
            if let order = self.order {
                cell.config(data: order, isCameFromViewOrderButton: isComeFromViewOrderButton, amountCollected: collectedAmount, isStarchApplied: isStarchApplied, mapVc: mapViewController!)
            }
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 2 {
            
            if self.order.orderItems[indexPath.row].isLocaleAdded {
                return true
            }
        }
        return false
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (action, view, completion) in
            
            var price = 0
            
            for item in self.order.orderItems {
                
                if item.isLocaleAdded {
                    price = item.price * item.quantity
                }
            }
            self.order.orderItems.remove(at: indexPath.row)
            self.getCalcualtedPrice ()
            
        }
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    private func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCell.EditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            self.order.orderItems.remove(at: indexPath.row)
        }
    }
}


//MARK: - OrderBottomDetailTableViewCellDelegate
extension NewJobNotificationViewController: OrderBottomDetailTableViewCellDelegate {
    
    func starchTapped() {
        isStarchApplied.toggle()
        tableView.reloadData()
    }
    
    func addItemPressed() {
        let controller = ItemsViewController()
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: nil)
    }
    
    func addCollectedAmountPressed() {
        
        if order.orderDetail.paymentStatus && order.orderDetail.orderType.lowercased() == "delivery"  {
            let alert = UIAlertController(title: "", message: "Amount already paid at pickup", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        } else {
            let controller = CollectedAmountViewController()
            controller.orderDetailVC = self
            controller.orderId = order.orderDetail.orderId
            controller.customerId = order.orderDetail.customerId
            controller.typeOfOrder = self.order.orderDetail.orderType
            controller.collectedAmount = {(amount) in
                self.collectedAmount = amount
                self.paidStatus = true
                //print(self.paidStatus)
            }
            
            controller.addPressed = {
                //    self.confirmPressed()
            }
            
            controller.delegate = mapViewController
            controller.modalPresentationStyle = .overCurrentContext
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func confirmPressed(startDescription: String = "") {
        
        if  order.orderDetail.orderType.lowercased() == "delivery" {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        if order.orderDetail.orderCategory == "" {
            self.dismiss(animated: true, completion: nil)
            
        } else {
            var requestObj: [String: Any] = [:]
            requestObj["customer_id"] = self.order.orderDetail.customerId
            requestObj["order_id"] = self.order.orderDetail.orderId
            requestObj["starch_status"] = isStarchApplied
            requestObj["description"] = startDescription
            requestObj["collected_amount"] = collectedAmount
            requestObj["paid_status"] = self.paidStatus
            print(paidStatus)
          
            var items: [[String: Any]] = []
            
            for item in self.order.orderItems {
                var itemD : [String:Any] = [:]
                itemD["item_id"] = item.itemId
                itemD["price"] = item.price
                itemD["washing_method"] = item.washingMethod
                itemD["quantity"] = item.quantity
                items.append(itemD)
            }
            requestObj["items"] = items
            
            ConfirmOrder.confirmOrder(request: requestObj) { (result, error, status) in
                
                if error == nil {
                    self.isOrderConfirmed = true
                    self.dismiss(animated: true, completion: nil)
                    
                } else {
                    Utility.showAlertController(self, error?.localizedDescription ?? "")
                }
            }
        }
    }
}

