//
//  QuestionTableViewCell.swift
//  CleaningPal
//
//  Created by Mac on 21/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ExpandableLabel

protocol FAQDelegate: AnyObject {
    func showDescription (indexPath: IndexPath)
}

class QuestionTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var arrow: UIImageView!
    

    //MARK: - Variable
    var indexPath: IndexPath?
    weak var delegate: FAQDelegate?

    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - configure
    func configure (faq: FAQ) {
        titleLabel.text = faq.title
        descriptionLabel.text = faq.description
        descriptionView.isHidden = faq.isCollapsed
        self.layoutIfNeeded()
        
        arrow.transform = CGAffineTransform(scaleX: 1, y: faq.isCollapsed ? 1: -1)
    }
    
    
    //MARK: - Actions
    @IBAction func collapsePressed(_ sender: Any) {
        delegate?.showDescription(indexPath: indexPath!)
    }
    
    
    //MARK: - Methods
}
