//
//  DataManager.swift
//  CleaningPal-Driver
//
//  Created by Mac on 01/04/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import ObjectMapper

class DataManager {
    
    static let shared = DataManager()
    
    func setUser (user: String) {
        UserDefaults.standard.set(user, forKey: "user_data")
    }
    
    func getUser() -> UserData? {
        var user: UserData?

        if UserDefaults.standard.object(forKey: "user_data") != nil {
            user = Mapper<UserData>().map(JSONString:UserDefaults.standard.string(forKey: "user_data")!)
        }
        return user
    }
    
    func deleteUser () {
         UserDefaults.standard.set(nil, forKey: "user_data")
    }
    
    func setAuthentication (auth: String) {
        UserDefaults.standard.set(auth, forKey: "auth_data")
    }
    
    func setBaseURL (url: String) {
        UserDefaults.standard.set(url, forKey: "base_url_data")
    }
    
    func getBaseURL() -> String? {
        var user: String?

        if UserDefaults.standard.object(forKey: "base_url_data") != nil {
            user = UserDefaults.standard.string(forKey: "base_url_data")!
        }

        return user
    }
    
    
//    func getAuthentication() -> AuthLogin? {
//        var auth: AuthLogin?
//        
//        if UserDefaults.standard.object(forKey: "auth_data") != nil {
//            auth = Mapper<AuthLogin>().map(JSONString:UserDefaults.standard.string(forKey: "auth_data")!)
//        }
//        return auth
//    }
    
    func deleteAuthentication () {
        UserDefaults.standard.set(nil, forKey: "auth_data")
    }
    
}
