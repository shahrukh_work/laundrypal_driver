//
//  Protocols.swift
//  CleaningPal-Driver
//
//  Created by Mac on 17/03/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import UIKit

protocol CollectedAmountDelegate: AnyObject {
    func addCollectedAmount (amount: Double)
    func paidStatus (status: Bool) 
}

protocol OnJobViewDelegate: AnyObject {
    func cancelTapped()
    func statusButtonTapped()
    func viewOrderTapped()
}

protocol ConfirmDelegate: AnyObject {
    func confirmTapped()
}
protocol PaidStatusDelegate: AnyObject {
    func paidStatus(status: Bool)
}

protocol CategoryDelegate: AnyObject {
    func plusButtonTapped(indexPath: IndexPath)
}

protocol ItemServiceSelectionViewControllerDelegate: AnyObject {
    func addPressed(data: OrderItems)
}

protocol OrderBottomDetailTableViewCellDelegate: AnyObject {
    func addItemPressed()
    func addCollectedAmountPressed()
    func starchTapped()
    func confirmPressed(startDescription: String)
}

protocol JobsTableTableViewCellDelegate: AnyObject {
    func startPressed(indexPath: IndexPath)
}

protocol CurrentJobTableViewCellDelegate: AnyObject {
    func inProgressPressed(indexPath: IndexPath)
}

protocol CancelingNoteViewControllerDelegate: AnyObject {
    func okPressed(message: String)
}

protocol NotAppearedNoteViewControllerDelegate: AnyObject {
    func okButtonPressed(message: String)
    func cancelPressed()
}

protocol CategoryItemCellDelegate: AnyObject {
    func addTapped (indexPath: IndexPath)
    func serviceSelected (selectedServiceType: Int, indexPath: IndexPath)
}
