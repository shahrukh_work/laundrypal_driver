
import UIKit
import Alamofire
import ObjectMapper

class Connectivity {
    
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

let APIClientDefaultTimeOut = 40.0

class APIClient: APIClientHandler {
    
    fileprivate var clientDateFormatter: DateFormatter
    var isConnectedToNetwork: Bool?
    
    static var shared: APIClient = {
        let baseURL = URL(fileURLWithPath: APIRoutes.baseUrl)
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = APIClientDefaultTimeOut
        
        let instance = APIClient(baseURL: baseURL, configuration: configuration)
        
        return instance
    }()
    
    // MARK: - init methods
    
    override init(baseURL: URL, configuration: URLSessionConfiguration, delegate: SessionDelegate = SessionDelegate(), serverTrustPolicyManager: ServerTrustPolicyManager? = nil) {
        clientDateFormatter = DateFormatter()
        
        super.init(baseURL: baseURL, configuration: configuration, delegate: delegate, serverTrustPolicyManager: serverTrustPolicyManager)
        
        //        clientDateFormatter.timeZone = NSTimeZone(name: "UTC")
        clientDateFormatter.dateFormat = "yyyy-MM-dd" // Change it to desired date format to be used in All Apis
    }
    
    
    // MARK: Helper methods
    
    func apiClientDateFormatter() -> DateFormatter {
        return clientDateFormatter.copy() as! DateFormatter
    }
    
    fileprivate func normalizeString(_ value: AnyObject?) -> String {
        return value == nil ? "" : value as! String
    }
    
    fileprivate func normalizeDate(_ date: Date?) -> String {
        return date == nil ? "" : clientDateFormatter.string(from: date!)
    }
    
    var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    func getUrlFromParam(apiUrl: String, params: [String: AnyObject]) -> String {
        var url = apiUrl + "?"
        
        for (key, value) in params {
            url = url + key + "=" + "\(value)&"
        }
        url.removeLast()
        return url
    }
    
    // MARK: - SignIn / SignUp
    
    func signInMethod(email: String , password: String, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["username": email, "password": password] as [String:String]
        
    }
    
    /*func forgotPasswordMethod(email: String, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["email": email] as [String:String]
        sendRequestUsingMultipart(APIRoutes.baseUrl + APIRoutes.forgotPassword , parameters: params as [String : AnyObject],httpMethod: .post , headers: nil, completionBlock: completionBlock)
    }
    
    func signInWebViewMethod(email: String , password: String, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["user": email, "pass": password] as [String:String]
        sendRequestUsingMultipart(APIRoutes.webViewBaseUrl + APIRoutes.signInWebView, parameters: params as [String : AnyObject] , httpMethod: .post, headers: nil, completionBlock: completionBlock)
    }*/
    
    @discardableResult
    func loginMethod (email: String , password: String, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["email": email, "password": password] as [String:AnyObject]
        return sendRequest(APIRoutes.login, parameters: nil, httpMethod: .post, headers: ["Content-Type":  "application/json", "email": email, "password": password] , completionBlock: completionBlock)
    }
        
    @discardableResult
    func logoutMethod (_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.logout, parameters: nil, httpMethod: .post, headers: ["Content-Type":  "application/json","Authorization": "Bearer "+UserData.shared.token] , completionBlock: completionBlock)
    }
    
    
    //MARK: - Home Screen
    @discardableResult
    func pendingOrderMethod(id: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let parameters = ["driver_id":id] as [String : AnyObject]
        return sendRequest(APIRoutes.pendingOrder, parameters: parameters, headers:["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func setNotificationsMethod(id: Int, status: Bool, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        let parameters = ["driver_id":id, "status": status] as [String : AnyObject]
        return sendRequest(APIRoutes.setNotifications, parameters: parameters, httpMethod: .post,headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func currentOrderMethod(id: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        let parameters = ["driver_id":id] as [String : AnyObject]
        return sendRequest(APIRoutes.currentOrder, parameters: parameters, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func completedOrderMethod(id: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        let parameters = ["driver_id":id] as [String : AnyObject]
        return sendRequest(APIRoutes.completedOrder, parameters: parameters, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    
    //MARK: - NewJobNotificationScreen
    @discardableResult
    func orderDetailMethod(id: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let parameters = ["order_id":id] as [String : AnyObject]
        return sendRequest(APIRoutes.orderDetail, parameters: parameters, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func driverArrivedMethod(orderId: Int, customerId: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        let parameters = ["order_id":orderId] as [String : AnyObject]
        return sendRequest(APIRoutes.driverArrived, parameters: parameters, httpMethod: .post, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getCategoriesListMethod( _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.getCategoriesList, parameters: nil, httpMethod: .get, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getSubCategoriesListMethod(catId: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
         let parameters = ["category_id":catId] as [String : AnyObject]
        return sendRequest(APIRoutes.getSubCategoriesList, parameters: parameters, httpMethod: .get, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getListItemsMethod(subCatId: Int, limit: Int, offset: Int = 0,_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let parameters = ["sub_category_id":subCatId,"limit":limit,"offset": offset] as [String : AnyObject]
        return sendRequest(APIRoutes.getItemsList, parameters: parameters, httpMethod: .get, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func setStartOrderMethod(orderId: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        let parameters = ["order_id":orderId] as [String : AnyObject]
        return sendRequest(APIRoutes.startOrder, parameters: parameters, httpMethod: .post, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func setStartJobMethod(jobId: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        let parameters = ["batch_id":jobId] as [String : AnyObject]
        return sendRequest(APIRoutes.startJob, parameters: parameters, httpMethod: .post, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func setCancelOrderMethod(orderId: Int, driverId: Int, cancelNote: String, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        let parameters = ["order_id":orderId, "driver_id": driverId, "cancel_note": cancelNote] as [String : AnyObject]
        return sendRequest(APIRoutes.cancelJob, parameters: parameters, httpMethod: .post, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func addEasyOrderMethod(easyOrderRequest: [String: Any], _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let parameters = easyOrderRequest as [String : AnyObject]
        return sendRequest(APIRoutes.confirmOrder, parameters: parameters, httpMethod: .post, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func setCompleteJob(jobId: Int,_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let parameters = ["batch_id":jobId] as [String : AnyObject]
        return sendRequest(APIRoutes.completeJob, parameters: parameters, httpMethod: .post, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func setOrderDeliveredMethod(orderId: Int, collectedAmount: Double, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let parameters = ["order_id":orderId, "amount": collectedAmount] as [String : AnyObject]
        return sendRequest(APIRoutes.orderDelivered, parameters: parameters, httpMethod: .post, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func setOrderPickedUpMethod(orderId: Int,_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let parameters = ["order_id":orderId] as [String : AnyObject]
        return sendRequest(APIRoutes.orderPickedUp, parameters: parameters, httpMethod: .post, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func updateTokenMethod(token: String,_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let parameters = ["fcm_token":token] as [String : AnyObject]
        return sendRequest(APIRoutes.updateToken, parameters: parameters, httpMethod: .post, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getNotificationListMethod(limit: Int = 10, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let parameters = ["limit":limit] as [String : AnyObject]
        return sendRequest(APIRoutes.notificationList, parameters: parameters, httpMethod: .get, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func searchItemsMethod(searchText: String, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let parameters = ["search":searchText] as [String : AnyObject]
        return sendRequest(APIRoutes.searchItems, parameters: parameters, httpMethod: .get, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getOnlineAdminMethod(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.onlineAdmin, parameters: nil, httpMethod: .get, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getChatTiming(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.chatTiming, parameters: nil, httpMethod: .get, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getChatHistoryMethod(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.chatHistory, parameters: nil, httpMethod: .get, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func updateLocationMethod(location: String,_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let parameters = ["location":location] as [String : AnyObject]
        return sendRequest(APIRoutes.userUpdateLoc, parameters: parameters, httpMethod: .post, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getEasyOrderMethod(orderID: Int,_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let parameters = ["order_id":orderID] as [String : AnyObject]
        return sendRequest(APIRoutes.getEasyOrderDetail, parameters: parameters, httpMethod: .post, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func gtsMethod(location: String,_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let parameters = ["location":location] as [String : AnyObject]
        return sendRequest(APIRoutes.userUpdateLoc, parameters: parameters, httpMethod: .post, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func updateDistance(distance: String, orderID: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let parameters = ["distance":distance, "order_id":orderID] as [String : AnyObject]
        return sendRequest(APIRoutes.updateDistance, parameters: parameters, httpMethod: .post, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func calcualtePriceMethod (request: [String: Any],_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let parameters = request
        return sendRequest(APIRoutes.priceCalculation, parameters: parameters as [String : AnyObject], httpMethod: .post, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    
    @discardableResult
    func calculatePaymentMethod(orderId: Int, customerId: Int, receivedAmount: String,_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        let parameters = ["order_id":orderId, "customer_id":customerId, "recieved_payment": receivedAmount] as [String : AnyObject]
        return sendRequest(APIRoutes.calculatePayment, parameters: parameters, httpMethod: .post, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
   
    @discardableResult
    func clearNotificationMethod(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.clearNotification, parameters: nil, httpMethod: .post, headers: ["Authorization": "Bearer "+UserData.shared.token ,"Accept":  "application/json"], completionBlock: completionBlock)
    }
    
    //    @discardableResult
    //    func getTopChartEpisodes(_ completionBlock:@escaping APIClientCompletionHandler)->Request {
    //        let tokenString = "bearer "+User.shared.profileData!.token
    //        let params = [:] as [String:AnyObject]
    //        return sendRequest(APIRoutes.getTopChartEpisodes, parameters: params, headers:["Content-Type":  "application/json", "Authorization": tokenString], completionBlock: completionBlock)
    //    }
    
    
    
    
    //
    //extension UIImage {
    //    func rotate(radians: Float) -> UIImage? {
    //        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
    //        // Trim off the extremely small float value to prevent core graphics from rounding it up
    //        newSize.width = floor(newSize.width)
    //        newSize.height = floor(newSize.height)
    //
    //        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
    //        let context = UIGraphicsGetCurrentContext()!
    //
    //        // Move origin to middle
    //        context.translateBy(x: newSize.width/2, y: newSize.height/2)
    //        // Rotate around middle
    //        context.rotate(by: CGFloat(radians))
    //        // Draw the image at its center
    //        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))
    //
    //        let newImage = UIGraphicsGetImageFromCurrentImageContext()
    //        UIGraphicsEndImageContext()
    //
    //        return newImage
    //    }
}

